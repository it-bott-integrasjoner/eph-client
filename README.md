# Eph-client

Python client for accessing ephorte.

## Ephorte

Ephorte is a "saksbehandling and arkiv" system which is used in the universities.
The ephorte has a integration component, EiS. This component is used in this integration.

At the moment, EiS is used for UiB, UiO, UiT, and NTNU.  
In order to consume the service following are required:
- gravitee with EiS credentials 
- sshutle with uio account
  
### UiO
- vpn is required for running integration test (ip level restriction)
- How to setup [VPN](https://www.uio.no/tjenester/it/nett/vpn/)
## Test

### Unit test: 
```python -m pytest``` 

### Integration test
Required to have all configurations are set in `config.yaml` locally.   
- Run all tests. `python -m pytest -m integration`
- Run one specific test: `python -m pytest -m integration -k test_name`
- Run with xml logging: `python -m  pytest -m integration -k test_fetch_sak --log-cli-level=DEBUG` 

One can maintain different config locally by having for example `config-uio.yaml` and `config-uib.yaml`.

UiO: VPN login (Cisco any connect) is required to run (not enough with sshutle).  

When there is new database (migration is done), normally the ids are required to be updated, so some tests will fail.  
Specially `def_saksbehandler_pn_id` is required to be updated. The saksbehandler is the integration user itself.  

## EiS
### Object model service
To check the domain model in Ephorte (Sak, Journalpost and etc) use `xsd/eis-object-model.xsd`.  
To search, create, delete Sak, Jounalpost

UiB: <https://gw-uib.intark.uh-it.no/eis/test/nCore/Services/objectmodel/v3/no/ObjectModelService.svc>  
UiO: <https://gw-uio.intark.uh-it.no/eis/test/nCore/services/ObjectModel/v3/no/ObjectModelService.svc?wsdl>

### Document service

To upload documents
UiB: <https://gw-uib.intark.uh-it.no/eis/test/nCore/Services/documents/V3/DocumentService.svc?wsdl>  
UiO: <https://gw-uio.intark.uh-it.no/eis/test/nCore/services/documents/v3/DocumentService.svc?wsdl>

## Ephorte web

IE is required for ephorte web (firefox and other web browser may not work properly)
### Ephorte web kurs (test) 
UiB: https://eph-uib-kurs.uhad.no (required to have uib user account with vpn access)  
UiO: https://eph-uio-kurs.uhad.no/ephorte (possible to enter via view.uio.no -> kontor -> IE )


## Ephorte search
The search is behave differently since UiO has different version(4.x) of EiS 
compared to UiB (6.x).


## References
[Ephorte production spesification (old version)](https://teams.microsoft.com/_#/school/files/General?threadId=19%3Abdea4527b6c44ea7956f2ab9cab618be%40thread.tacv2&ctx=channel&context=old-eis-doc&rootfolder=%252Fsites%252FBOTTintegrasjonsutviklingsteam%252FDelte%2520dokumenter%252FGeneral%252FTOA%252FeIS%2520(filer%2520fra%2520Frank%2520S%25C3%25B8rensen%2520hos%2520Sikri)%252Fold-eis-doc)
