import datetime
from datetime import date, timedelta
from unittest.mock import Mock

import pytest

from eph_client import ClientError
from eph_client.models import (
    AvsenderMottakerPatch,
    CreateSakReq,
    EphAdministrativEnhet,
    EphAvsenderMottaker,
    EphJournalpost,
    EphKlassering,
    EphSak,
    JpPatch,
    Person,
    Search,
    Sek_klassering,
    to_date,
)

sek_klassering = Sek_klassering(
    ordnings_verdi_id="212.22",
    beskrivelse="Midlt. tilsetting i øvrige stillingskategorier, engasjementer i ledige "
    "stillinger, åremål o.l.",
)

tittel = (
    "Hansen, Ola - Tilsettingssak - Professor ved Seksjon for "
    "dokumentasjonsforvaltning"
)
off_tittel = (
    "***** ***** - Tilsettingssak - Professor ved Seksjon for "
    "dokumentasjonsforvaltning"
)

person = Person(
    first_name="Ola",
    last_name="Hansen",
    emp_nr="1223",
    email="email@email.no",
    post_address="street 1",
    post_code="2233",
    post_place="place",
    birth_date="1977-01-18",
)

adm_enhet = EphAdministrativEnhet(
    betegnelse="Seksjon for dokumentasjonsforvaltning",
    journal_enhet_id="sd-02",
    kortnavn="12345",
    id=2,
)


def test_parse_date():
    date = to_date("01.01.2020")
    assert date is not None
    assert date.year == 2020
    assert date.month == 1
    assert date.day == 1


def test_create_sak_without_emp_nr(empty_client):
    with pytest.raises(ClientError) as tx_error:
        empty_client.create_sak(
            CreateSakReq(
                person=person,
                sek_klassering=sek_klassering,
                dokument_beskrivelse="jp innhold",
                adm_enhet=adm_enhet,
                dokument_dato=date.today().isoformat(),
                jp_patch=JpPatch(),
                tittel=tittel,
                off_tittel=off_tittel,
            )
        )

        assert tx_error.value.args[0] == "object API is not configured"


def test_create_sak_saksbehandler_not_called_w_saksbehandler(client_example):
    client_example.config.jp_config.avskriving_maate = None
    client_example.search_saksbehandler = Mock()
    client_example.get_saksbehandler_pn_id = Mock(return_value=0)

    mock_client_insert_update(client_example)

    client_example.create_sak(
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
        )
    )

    client_example.search_saksbehandler.assert_not_called()


def test_create_sak_saksbehandler(client_example):
    client_example.get_saksbehandler_pn_id = Mock(return_value=42)
    client_example.avskriv_restansen = Mock()
    client_example._do_search = Mock()
    client_example.update_avs_mot = Mock()

    mock_client_insert_update(client_example)

    client_example.create_sak(
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
        )
    )
    client_example.avskriv_restansen.assert_called_once()
    client_example.update_avs_mot.assert_called_once()


def test_create_sak_saksbehandler_wout_avskriving(client_example):
    client_example.config.jp_config.avskriving_maate = None
    client_example.get_saksbehandler_pn_id = Mock(return_value=42)
    client_example.avskriv_restansen = Mock()
    client_example._do_search = Mock()
    client_example.update_avs_mot = Mock()

    mock_client_insert_update(client_example)

    client_example.create_sak(
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
        )
    )
    client_example.avskriv_restansen.assert_not_called()
    client_example.update_avs_mot.assert_called()


def test_create_avsender_mottaker__without_patch(client_example):
    class MockModelFactory:
        class AvsenderMottaker(EphAvsenderMottaker):
            pass

    mock_model_factory = MockModelFactory()
    ref_jp = 1

    avs_mot = client_example.create_avsender_mottaker(
        mock_model_factory,
        ref_jp,
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
        ),
    )

    assert avs_mot.AdministrativEnhetId is None
    assert avs_mot.Innholdstype is None
    assert avs_mot.KopimottakerMedavsender is None


def test_create_avsender_mottaker__with_patch(client_example):
    class MockModelFactory:
        class AvsenderMottaker(EphAvsenderMottaker):
            pass

    mock_model_factory = MockModelFactory()
    ref_jp = 1
    avsender_mottaker_patch = AvsenderMottakerPatch(
        AdministrativEnhetId=1,
        Innholdstype=True,
        KopimottakerMedavsender=True,
    )

    avs_mot = client_example.create_avsender_mottaker(
        mock_model_factory,
        ref_jp,
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
        ),
        avsender_mottaker_patch,
    )

    assert avs_mot.AdministrativEnhetId == 1
    assert avs_mot.Innholdstype is True
    assert avs_mot.KopimottakerMedavsender is True


def test_create_multiple_avsender_mottaker__without_patch(client_example):
    class MockModelFactory:
        class AvsenderMottaker(EphAvsenderMottaker):
            pass

    mock_model_factory = MockModelFactory()
    ref_jp = 1

    avs_mots = client_example.create_multiple_avsender_mottaker(
        mock_model_factory,
        ref_jp,
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
        ),
    )

    assert len(avs_mots) == 1
    avs_mot = avs_mots[0]
    assert avs_mot.AdministrativEnhetId is None
    assert avs_mot.Innholdstype is None
    assert avs_mot.KopimottakerMedavsender is None


def test_create_multiple_avsender_mottaker__with_empty_patch(client_example):
    class MockModelFactory:
        class AvsenderMottaker(EphAvsenderMottaker):
            pass

    mock_model_factory = MockModelFactory()
    ref_jp = 1
    avsender_mottaker_patch: list[AvsenderMottakerPatch] = []

    avs_mots = client_example.create_multiple_avsender_mottaker(
        mock_model_factory,
        ref_jp,
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
            avsender_mottaker_patch=avsender_mottaker_patch,
        ),
    )

    assert len(avs_mots) == 1
    avs_mot = avs_mots[0]
    assert avs_mot.AdministrativEnhetId is None
    assert avs_mot.Innholdstype is None
    assert avs_mot.KopimottakerMedavsender is None


def test_create_multiple_avsender_mottaker__with_patch(client_example):
    class MockModelFactory:
        class AvsenderMottaker(EphAvsenderMottaker):
            pass

    mock_model_factory = MockModelFactory()
    ref_jp = 1
    avsender_mottaker_patch = [
        AvsenderMottakerPatch(
            AdministrativEnhetId=1,
            Innholdstype=True,
            KopimottakerMedavsender=True,
        )
    ]

    avs_mots = client_example.create_multiple_avsender_mottaker(
        mock_model_factory,
        ref_jp,
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
            avsender_mottaker_patch=avsender_mottaker_patch,
        ),
    )

    assert len(avs_mots) == 1
    avs_mot = avs_mots[0]
    assert avs_mot.AdministrativEnhetId == 1
    assert avs_mot.Innholdstype is True
    assert avs_mot.KopimottakerMedavsender is True


def test_create_multiple_avsender_mottaker__with_multiple_patches(
    client_example,
):
    class MockModelFactory:
        class AvsenderMottaker(EphAvsenderMottaker):
            pass

    mock_model_factory = MockModelFactory()
    ref_jp = 1
    avsender_mottaker_patch = [
        AvsenderMottakerPatch(
            AdministrativEnhetId=1,
            Innholdstype=True,
            KopimottakerMedavsender=True,
        ),
        AvsenderMottakerPatch(
            AdministrativEnhetId=2,
            Innholdstype=True,
            KopimottakerMedavsender=True,
        ),
    ]

    avs_mots = client_example.create_multiple_avsender_mottaker(
        mock_model_factory,
        ref_jp,
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
            avsender_mottaker_patch=avsender_mottaker_patch,
        ),
    )

    assert len(avs_mots) == 2
    avs_mot = avs_mots[0]
    assert avs_mot.AdministrativEnhetId == 1
    assert avs_mot.Innholdstype is True
    assert avs_mot.KopimottakerMedavsender is True

    avs_mot = avs_mots[1]
    assert avs_mot.AdministrativEnhetId == 2
    assert avs_mot.Innholdstype is True
    assert avs_mot.KopimottakerMedavsender is True


def test_create_multiple_avsender_mottaker__set_field_to_None(
    client_example,
):
    class MockModelFactory:
        class AvsenderMottaker(EphAvsenderMottaker):
            pass

    mock_model_factory = MockModelFactory()
    ref_jp = 1
    avsender_mottaker_patch = AvsenderMottakerPatch(
        Person=None,
        AdministrativEnhetId=1,
        Innholdstype=True,
        KopimottakerMedavsender=True,
    )

    avs_mot = client_example.create_avsender_mottaker(
        mock_model_factory,
        ref_jp,
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today().isoformat(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
        ),
        avsender_mottaker_patch,
    )

    assert avs_mot.Journalpost == 1
    assert avs_mot.Person == None
    assert avs_mot.AdministrativEnhetId == 1
    assert avs_mot.Innholdstype is True
    assert avs_mot.KopimottakerMedavsender is True


def test_search_parameter():
    search_config = Search(
        title="Ola hansen",
        adm_enh_id=100,
        status_avsluttet="dd",
        status_utgaat="dd",
        prim_ord_prinsipp="ss",
        sek_ord_prinsipp="sd",
        ord_verdi_id="ss",
    )
    assert search_config.title == "Ola hansen"


def test_search_parameter_with_title_from_test():
    tittle = "Spr\u00e5k, Sindre - Tilsettingssak - Vit. assistent ved HR-avdelingen"
    search_config = Search(
        title=str(tittle),
        adm_enh_id=100,
        status_avsluttet="dd",
        status_utgaat="dd",
        prim_ord_prinsipp="ss",
        sek_ord_prinsipp="dd",
        ord_verdi_id="ss",
    )
    assert (
        search_config.title
        == "Språk, Sindre - Tilsettingssak - Vit. assistent ved HR-avdelingen"
    )


def test_create_sak_without_object_service(empty_client):
    with pytest.raises(ClientError, match="object and document API is not configured"):
        empty_client.create_sak(
            CreateSakReq(
                person=person,
                sek_klassering=sek_klassering,
                dokument_beskrivelse="jp innhold",
                adm_enhet=adm_enhet,
                dokument_dato=date.today(),
                jp_patch=JpPatch(),
                tittel=tittel,
                off_tittel=off_tittel,
            )
        )


def test_get_doc_format__format_provided(client_example, test_doc):
    test_doc.name = "simple"
    test_doc.format = "application/pdf"
    eph_format = client_example.get_doc_format(test_doc)
    assert eph_format == "RA-PDF"


def test_get_doc_format__format_guessed(client_example, test_doc):
    eph_format = client_example.get_doc_format(test_doc)
    assert eph_format == "RA-PDF"


def test_get_doc_format__format_from_file_extension(client_example, test_doc):
    test_doc.name = "simple.xml"
    eph_format = client_example.get_doc_format(test_doc)
    assert eph_format == "XML"


def test_create_sak_multiple_documents(client_example, test_doc):
    client_example.get_saksbehandler_pn_id = Mock(return_value=0)
    client_example.config.jp_config.avskriving_maate = None
    client_example.create_document = Mock()
    client_example.create_document.return_value = []

    mock_client_insert_update(client_example)

    docs = [test_doc, test_doc]
    client_example.create_sak(
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today(),
            jp_patch=JpPatch(),
            tittel=tittel,
            off_tittel=off_tittel,
            docs=docs,
        )
    )

    assert client_example.create_document.call_count == len(docs)


@pytest.mark.parametrize(
    ("input", "output"),
    [
        (
            (date(year=2024, month=7, day=11), date(year=2024, month=7, day=11)),
            [],
        ),
        (
            (date(year=2024, month=7, day=10), date(year=2024, month=7, day=11)),
            [
                (date(year=2024, month=7, day=10), date(year=2024, month=7, day=10)),
                (date(year=2024, month=7, day=11), date(year=2024, month=7, day=11)),
            ],
        ),
        (
            (date(year=2024, month=7, day=10), date(year=2024, month=7, day=12)),
            [
                (date(year=2024, month=7, day=10), date(year=2024, month=7, day=11)),
                (date(year=2024, month=7, day=12), date(year=2024, month=7, day=12)),
            ],
        ),
        (
            (date(year=2024, month=6, day=12), date(year=2024, month=7, day=20)),
            [
                (date(year=2024, month=6, day=12), date(year=2024, month=7, day=1)),
                (date(year=2024, month=7, day=2), date(year=2024, month=7, day=20)),
            ],
        ),
    ],
)
def test_split_date_interval(client_example, input, output):
    assert client_example._split_date_interval(input[0], input[1]) == output


def test_search_jp_by_date_missing_dates(client_example, mock_api):
    match = f"Missing date_from: None or date_to: None"
    with pytest.raises(ValueError, match=match):
        client_example.search_jp_by_date([("tilleggsattributt1", "*")], None, None)


def test_search_jp_by_date_wrong_field_filter(client_example):
    with pytest.raises(ValueError, match="Invalid argument: fields"):
        client_example.search_jp_by_date([("tilleggsattributt1")], None, date.today())


def test_search_jp_by_date(client_example):
    today = date(year=2020, month=5, day=2)
    tomorrow = today + timedelta(days=1)
    client_example._do_search = Mock()
    client_example.search_jp_by_date([("tilleggsattributt1", "*")], None, today)

    client_example._do_search.assert_called_with(
        f"tilleggsattributt1='*' AND Opprettet <= '{tomorrow.strftime('%Y.%m.%d')}'",
        "Journalpost",
        EphJournalpost,
    )


def test_search_jp_by_date__too_many_rows(client_example):
    date_from = date(year=2024, month=7, day=2)
    date_to = date(year=2024, month=7, day=10)
    client_example._do_search = Mock()

    def do_search_side_effect(*args, **kwargs):
        if (
            args[0]
            == "tilleggsattributt1='*' AND Opprettet >= '2024.07.02' AND Opprettet <= '2024.07.07'"
        ):
            return [1, 2]
        elif (
            args[0]
            == "tilleggsattributt1='*' AND Opprettet >= '2024.07.07' AND Opprettet <= '2024.07.09'"
        ):
            return [2, 3]
        else:
            raise ClientError(
                "Search failed,  Original Exception: Filtered count query failed. Maximum number of rows reached (5000)."
            )

    client_example._do_search.side_effect = do_search_side_effect

    result = client_example.search_jp_by_date(
        [("tilleggsattributt1", "*")],
        date_from,
        date_to,
    )
    assert result == [1, 2, 2, 3]

    query_args = []
    for call in client_example._do_search.call_args_list:
        query_args.append(call.args[0])
    assert query_args == [
        "tilleggsattributt1='*' AND Opprettet >= '2024.07.02' AND Opprettet <= '2024.07.11'",
        "tilleggsattributt1='*' AND Opprettet >= '2024.07.02' AND Opprettet <= '2024.07.07'",
        "tilleggsattributt1='*' AND Opprettet >= '2024.07.07' AND Opprettet <= '2024.07.11'",
        "tilleggsattributt1='*' AND Opprettet >= '2024.07.07' AND Opprettet <= '2024.07.09'",
        "tilleggsattributt1='*' AND Opprettet >= '2024.07.09' AND Opprettet <= '2024.07.11'",
        "tilleggsattributt1='*' AND Opprettet >= '2024.07.09' AND Opprettet <= '2024.07.10'",
        "tilleggsattributt1='*' AND Opprettet >= '2024.07.10' AND Opprettet <= '2024.07.11'",
    ]


def test_upload_document_without_document_service(empty_client, test_doc):
    with pytest.raises(ClientError, match="object and document API is not configured"):
        empty_client.upload_file(test_doc)


def test_search_sak_without_object_service(empty_client):
    with pytest.raises(ClientError, match="object and document API is not configured"):
        empty_client.search_sak(title="test", emp_nr="22", adm_enh_id=100)


def test_search_sak(client_example):
    client_example.document_service = Mock()
    client_example.object_service = Mock()
    client_example.object_client = Mock()
    client_example._do_search = Mock()

    client_example.search_sak(title="test", emp_nr="22", adm_enh_id=100)
    query = (
        "tittel = 'test' AND SaksansvarligEnhetId ='100' AND SaksstatusId != 'A' "
        "AND SaksstatusId != 'U' AND Primaerklassering.OrdningsprinsippId = 'ANR' "
        "AND Primaerklassering.OrdningsverdiId = '22'"
    )
    client_example._do_search.assert_called_once_with(query, "Sak", EphSak, "Id desc")


def test_search_sak_uio(client_example):
    client_example.config.interface = "uio"
    client_example.config.sak_config.sek_ordningsprinsipp = "ANSATTNR"
    client_example.document_service = Mock()
    client_example.object_service = Mock()
    client_example.object_client = Mock()
    client_example._do_search = Mock()

    client_example.search_sak(title="test", emp_nr="22", adm_enh_id=100)
    query = (
        "tittel = 'test' AND SaksansvarligEnhetId ='100' AND SaksstatusId != 'A' "
        "AND SaksstatusId != 'U' AND Sekundaerklassering.OrdningsprinsippId = 'ANSATTNR' "
        "AND Sekundaerklassering.OrdningsverdiId = '22'"
    )
    client_example._do_search.assert_called_once_with(query, "Sak", EphSak, "Id desc")


def test_search_sak_uit(client_example):
    client_example.document_service = Mock()
    client_example.object_service = Mock()
    client_example.object_client = Mock()
    client_example._do_search = Mock()
    client_example.config.interface = "uit"
    client_example.config.sak_config.primaer_ordningsprinsipp_id = "221"

    client_example.search_sak(title="test", emp_nr="22", adm_enh_id=100)
    query = (
        "tittel = 'test'  AND SaksstatusId != 'A' "
        "AND SaksstatusId != 'U' AND Primaerklassering.OrdningsprinsippId = 'ANR' "
        "AND Primaerklassering.OrdningsverdiId = '221'"
    )
    client_example._do_search.assert_called_once_with(query, "Sak", EphSak, "Id desc")


def test_search_created_sak(client_example):
    client_example._do_search = Mock()
    client_example._do_search.return_value = []

    client_example.search_created_sak(2022, 1234)
    query = "Saksaar={} and Sekvensnummer={} and SaksstatusId != U and SaksstatusId != A".format(
        2022, 1234
    )

    client_example._do_search.assert_called_once_with(
        client_example.escape_query(query), "Sak", EphSak
    )


def test_get_klasseringer_by_sak_id(client_example):
    client_example._do_search = Mock()
    client_example._do_search.return_value = []

    client_example.get_klasseringer_by_sak_id(100)

    client_example._do_search.assert_called_with(
        f"SakId = 100",
        "Klassering",
        EphKlassering,
    )


def mock_client_insert_update(client_example):
    client_example.document_client = Mock()
    client_example.document_service = Mock()
    client_example.object_service = Mock()
    client_example.object_client = Mock()
    client_example.insert_or_update = lambda ins_obj, update=False: [
        Mock(wraps=EphSak, Tittel="tittel", Id=100),
        Mock(
            "jp",
            wraps=EphJournalpost,
            Id=1,
            Opprettet=datetime.datetime.now(),
            attr__Id="jp1",
        ),
        Mock("avsender_mottaker", wraps=EphAvsenderMottaker, Navn="Olsen"),
        Mock("prim_klassering", wraps=EphKlassering),
        Mock("sek_klassering", wraps=EphKlassering),
    ]
