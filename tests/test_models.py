from datetime import date

import pytest

from eph_client.models import (
    CreateSakReq,
    EphAdministrativEnhet,
    EphSak,
    JpPatch,
    Person,
    SakPatch,
    Sek_klassering,
)

sek_klassering = Sek_klassering(
    ordnings_verdi_id="212.22",
    beskrivelse="Midlt. tilsetting i øvrige stillingskategorier, "
    "engasjementer i ledige stillinger, åremål o.l.",
)

person = Person(
    first_name="Ola",
    last_name="Hansen",
    emp_nr="1223",
    email="email@email.no",
    post_address="street 1",
    post_code="2233",
    post_place="place",
    birth_date="1977-01-18",
)

adm_enhet = EphAdministrativEnhet(
    betegnelse="Seksjon for dokumentasjonsforvaltning",
    journal_enhet_id="sd-02",
    kortnavn="12345",
    id=2,
)

jp_patch = JpPatch(
    avskriving_maate="_TE",
    arkivdel_id="_PESONAL",
    avsender_untatt_off=False,
    dokument_type_id="_U",
    hjemmel="_offl. § 26, 5. ledd",
    status="_B",
    dokumentkategori_id="_ND",
    tilgangskode_id="_P",
    tilleggsattributt1="_1",
    tilleggsattributt2="_2",
    tilleggsattributt3="_3",
    tilleggsattributt4="_4",
    tilleggsattributt5="_5",
    tilleggsattributt10="_10",
)

sak_patch = SakPatch(
    arkivdel_id="_PERSONAL",
    primaer_ordningsprinsipp="ARKNOK",
    sek_ordningsprinsipp="ANR",
    hjemmel="_offl. § 35, 7. ledd",
    tilgangskode_id="_P",
    tilgangsgruppe_id="234",
)


def test_models_create_sak_journalpost_def_value(example_config):
    example_config.jp_config.avskriving_maate = None
    req = CreateSakReq(
        tittel="tittel",
        person=person,
        sek_klassering=sek_klassering,
        dokument_beskrivelse="jp innhold",
        adm_enhet=adm_enhet,
        dokument_dato=date.today(),
        jp_patch=JpPatch(),
        off_tittel="off_tittel",
    )

    req.apply_def_config(example_config.jp_config, "jp_patch")

    assert req
    for field in req.jp_patch.keys():
        if type(req.jp_patch[field]) == bool or not req.jp_patch[field]:
            continue
        assert (
            req.jp_patch[field] != jp_patch[field]
        ), f"expected field: {field} should be equal"


def test_apply_def_config_journalpost_overridden_values(example_config):
    req = CreateSakReq(
        person=person,
        sek_klassering=sek_klassering,
        dokument_beskrivelse="jp innhold",
        adm_enhet=adm_enhet,
        dokument_dato=date.today(),
        jp_patch=jp_patch,
        tittel="tittel",
        off_tittel="off_tittel",
    )

    req.apply_def_config(example_config.jp_config, "jp_patch")

    assert req is not None
    for fields in req.jp_patch.keys():
        assert req.jp_patch[fields] == jp_patch[fields]


def test_apply_def_config_journalpost_has_def_values(example_config):
    # empty journal patch (nothing to update)
    jp_patch_empty = JpPatch()

    req = CreateSakReq(
        person=person,
        sek_klassering=sek_klassering,
        dokument_beskrivelse="jp innhold",
        adm_enhet=adm_enhet,
        dokument_dato=date.today(),
        jp_patch=jp_patch_empty,
        tittel="tittel",
        off_tittel="off_tittel",
    )

    req.apply_def_config(example_config.jp_config, "jp_patch")

    # assert that req.jounalpost has values from config
    assert req is not None
    for field in example_config.jp_config.keys():
        assert req.jp_patch[field] == example_config.jp_config[field]


def test_models_create_sak_sak_def_value(example_config):
    # example_config.jp_config.avskriving_maate = None
    req = CreateSakReq(
        tittel="tittel",
        person=person,
        sek_klassering=sek_klassering,
        dokument_beskrivelse="jp innhold",
        adm_enhet=adm_enhet,
        dokument_dato=date.today(),
        jp_patch=JpPatch(),
        sak_patch=SakPatch(),
        off_tittel="off_tittel",
    )

    req.apply_def_config(example_config.sak_config, "sak_patch")

    assert req
    for field in req.sak_patch.keys():
        if type(req.sak_patch[field]) == bool or not req.sak_patch[field]:  # type: ignore[index]
            continue
        assert (
            req.sak_patch[field] != sak_patch[field]  # type: ignore[index]
        ), f"expected field: {field} should be equal"


def test_apply_def_config_sak_overridden_values(example_config):
    req = CreateSakReq(
        person=person,
        sek_klassering=sek_klassering,
        dokument_beskrivelse="jp innhold",
        adm_enhet=adm_enhet,
        dokument_dato=date.today(),
        jp_patch=JpPatch(),
        sak_patch=sak_patch,
        tittel="tittel",
        off_tittel="off_tittel",
    )

    req.apply_def_config(example_config.sak_config, "sak_patch")

    assert req is not None
    for field in req.sak_patch.keys():
        if sak_patch[field] is None:
            continue
        assert req.sak_patch[field] == sak_patch[field]  # type: ignore[index]


def test_apply_def_config_sak_has_def_values(example_config):
    # empty sak patch (nothing to update)
    sak_patch_empty = SakPatch()

    req = CreateSakReq(
        person=person,
        sek_klassering=sek_klassering,
        dokument_beskrivelse="jp innhold",
        adm_enhet=adm_enhet,
        dokument_dato=date.today(),
        jp_patch=JpPatch(),
        sak_patch=sak_patch_empty,
        tittel="tittel",
        off_tittel="off_tittel",
    )

    req.apply_def_config(example_config.sak_config, "sak_patch")

    # assert that req.sak has values from config
    assert req is not None
    for field in example_config.sak_config.keys():
        assert req.sak_patch[field] == example_config.sak_config[field]  # type: ignore[index]


def test_def_config(example_config):
    """Test default config"""

    assert example_config.interface
    assert example_config.use_pre_created_sak is False


def test_def_config_uib(example_config_uib):
    """Test config for uib
    - uses pre_created_sak: sak is searched by sakaar and sekvensnr
      when found a sak, that sak is updated.
    """

    assert example_config_uib.interface
    assert example_config_uib.use_pre_created_sak is True
    assert example_config_uib.jp_config.avskriving_maate == "TE"


def test_def_config_uio(example_config_uio):
    """Test config for uio"""

    assert example_config_uio.interface
    assert example_config_uio.use_pre_created_sak is False
    assert example_config_uio.jp_config.avskriving_maate == "TE"


def test_def_config_uit(example_config_uit):
    """Test config for uit"""

    assert example_config_uit.interface
    assert example_config_uit.jp_config.avskriving_maate == "TA"
    assert example_config_uit.use_pre_created_sak is False
    assert example_config_uit.sak_config.sek_ordningsprinsipp is None


def test_create_sak_req_raises_validation_error():
    error_msg = f"saksaar: <None> or sekvensnummer: <None> is missing"
    with pytest.raises(ValueError, match=error_msg):
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today(),
            jp_patch=jp_patch,
            tittel="tittel",
            off_tittel="off_tittel",
            sak_patch_fields={"Tittel"},
        )


def test_create_sak_req_sak_patch_is_none():
    sak_req = CreateSakReq(
        person=person,
        sek_klassering=sek_klassering,
        dokument_beskrivelse="jp innhold",
        adm_enhet=adm_enhet,
        dokument_dato=date.today(),
        jp_patch=jp_patch,
        tittel="tittel",
        off_tittel="off_tittel",
    )

    assert sak_req.sak_patch_fields is None
    assert sak_req.saksaar is None
    assert sak_req.sak_patch_fields is None


def test_create_sak_req_raises_validation_error_invalid_field_for_sak_patch():
    error_msg = f"Invalid field: foo in sak_patch_fields"
    with pytest.raises(ValueError, match=error_msg):
        CreateSakReq(
            person=person,
            sek_klassering=sek_klassering,
            dokument_beskrivelse="jp innhold",
            adm_enhet=adm_enhet,
            dokument_dato=date.today(),
            jp_patch=jp_patch,
            tittel="tittel",
            off_tittel="off_tittel",
            saksaar=2022,
            sekvensnummer=123,
            sak_patch_fields={"foo"},
        )


def test_create_sak_req():
    create_req = CreateSakReq(
        person=person,
        sek_klassering=sek_klassering,
        dokument_beskrivelse="jp innhold",
        adm_enhet=adm_enhet,
        dokument_dato=date.today(),
        jp_patch=jp_patch,
        tittel="tittel",
        off_tittel="off_tittel",
        saksaar=2022,
        sekvensnummer=123,
    )
    assert create_req.saksaar == 2022
    assert create_req.sekvensnummer == 123
