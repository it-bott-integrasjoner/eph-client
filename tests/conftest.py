import logging
import logging.config
from pathlib import Path

import pytest
import requests_mock
import yaml

from eph_client import EphClient, get_client
from eph_client.models import Doc, EphClientConfig


@pytest.fixture
def empty_client():
    config = EphClientConfig(interface="uib", username="uibint")
    config.document_service = None
    config.object_service = None

    return get_client(config)


@pytest.fixture
def client(service_config: EphClientConfig):
    client = get_client(service_config)

    add_logging()
    return client


@pytest.fixture
def client_example(example_config: EphClientConfig):
    example_client = EphClient(example_config, init_soap=False)
    return example_client


# Used in integration tests, default: :disabled
@pytest.fixture
def service_config():
    file = load_file("./config.yaml")
    configs = yaml.load(file, Loader=yaml.FullLoader)
    return EphClientConfig(**configs)


# Used in integration tests, default: :disabled
@pytest.fixture
def example_config():
    file = load_file("./config.example.yaml")
    configs = yaml.load(file, Loader=yaml.FullLoader)
    return EphClientConfig(**configs)


@pytest.fixture
def example_config_uib():
    return get_eph_client_config("./config.example-uib.yaml")


@pytest.fixture
def example_config_uio():
    return get_eph_client_config("./config.example-uio.yaml")


@pytest.fixture
def example_config_uit():
    return get_eph_client_config("./config.example-uit.yaml")


@pytest.fixture
def test_doc():
    content = load_file("./tests/res/simple.pdf", "rb")
    content = bytes(content)
    return Doc(name="simple.pdf", title="simple pdf file", content=content)


def get_eph_client_config(file_name):
    file = load_file(file_name)
    configs = yaml.load(file, Loader=yaml.FullLoader)
    return EphClientConfig(**configs)


def load_file(file, rb="r"):
    file = Path(file)
    if not file.exists():
        raise FileNotFoundError("Missing file: " + str(file))
    with open(file, rb) as f:
        return f.read()


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)


def add_logging():
    logging.config.dictConfig(
        {
            "version": 1,
            "formatters": {"verbose": {"format": "%(name)s: %(message)s"}},
            "handlers": {
                "console": {
                    "level": "DEBUG",
                    "class": "logging.StreamHandler",
                    "formatter": "verbose",
                },
            },
            "loggers": {
                "zeep.transports": {
                    "level": "DEBUG",
                    "propagate": True,
                    "handlers": ["console"],
                },
            },
        }
    )


@pytest.fixture
def mock_api():
    with requests_mock.Mocker() as mock:
        yield mock
