"""Inegration test for ephorte-client
   Requires access to EiS (uib vpn) """

import json
import logging
from copy import deepcopy
from datetime import date, datetime

import pytest

from eph_client import get_client
from eph_client.models import (
    AvsenderMottakerPatch,
    CreateSakReq,
    Doc,
    EphAdministrativEnhet,
    EphAvsenderMottaker,
    EphClientConfig,
    EphJournalpost,
    EphKlassering,
    EphPerson,
    EphPersonNavn,
    EphSak,
    JpPatch,
    Person,
    Sek_klassering,
)

"""
integrations tests:  manually started
To run:
    - Set credentials in config.yaml
    - python -m pytest -m integration
"""

logger = logging.getLogger(__name__)

data: dict[str, str | int | bool] = {
    "api_user_uib": "KONNEKTOR",
    "api_user_uio": "EIS-API",
    "api_user_uit": "TOAIMP",
    "adm_id_uio": 418,
    "adm_id_uib": 100,
    "adm_id_uit": 19,
    "sak_id_uio": 250040,
    "sak_id_uib": 250776,
    "sak_id_uit": 46204,
    "saksbehandler_uio": 48681,
    "saksbehandler_uib": 19010,
    "saksbehandler_uit": 4257,
    "sak_id_pre_created_uib": 251114,
    "sekvensnr_uib": "0000007782",
    "sekvensnr_uio": "0000007730",
    "jp_id_uio": 1642693,
    "jp_id_uib": 1176931,
    "jp_id_uit": 377987,
    "je_id_uio": "J-UIO",
    "je_id_uib": "DS01",
    "je_id_uit": "UIT",
    "sek_kl_verdi_uio": "212.2",
    "sek_kl_besk_uio": "TILSETTING I TEKNISK-ADMINISTRATIVE STILLINGER - FASTE OG ÅREMÅL - ("
    "også vikariater og konstitusjoner)",
    "sek_kl_verdi_uib": "212.22",
    "sek_kl_besk_uib": "Midlt. tilsetting i øvrige stillingskategorier, "
    "engasjementer i ledige stillinger, åremål o.l.",
    "avsender_untatt_off_uio": True,
    "avsender_untatt_off_uit": True,
    "sek_klassering_untatt_off_uio": True,
    "sek_klassering_untatt_off_uit": True,
}

emp_nr = "00102722"

jp_tittel = "Signert arbeidsavtale 01.06.2020 - 13.09.2020"
person = Person(
    first_name="Ola",
    last_name="Hansen",
    emp_nr=emp_nr,
    email="email@email.no",
    post_address="street 1",
    post_code="2233",
    birth_date="1977-01-18",
)
kontrakt_type = "Månedskontrakt"


@pytest.mark.integration
def test_config_interface_should_have_correct_value(service_config):
    avmot_untatt_off = _get_config_value(service_config, "avsender_untatt_off", False)
    sek_untatt_off = _get_config_value(
        service_config, "sek_klassering_untatt_off", False
    )
    assert avmot_untatt_off == service_config.jp_config.avsender_untatt_off
    assert sek_untatt_off == service_config.sak_config.sek_klassering_untatt_off


@pytest.mark.integration
def test_upload_file(client, test_doc):
    file_save_ref = client.upload_file(test_doc)
    assert file_save_ref is not None
    assert file_save_ref.startswith("UPLOAD_{")


@pytest.mark.integration
def test_update_sak_w_new_jp_w_file(client, test_doc, service_config):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)
    sak_id = data["sak_id_" + service_config.interface]
    test_doc.name = (
        "Signert arbeidsavtale (Timekontrakt) - Ukjent org(10000083) - "
        "05.06.2021-22.12.2021.pdf"
    )
    sak_req = CreateSakReq(
        sak_id=sak_id,
        tittel=tittel,
        off_tittel=off_tittel,
        person=person,
        dokument_beskrivelse=jp_tittel,
        sek_klassering=get_sek_klassering(service_config),
        docs=[test_doc],
        adm_enhet=get_adm_enhet(service_config),
        dokument_dato=date(2020, 1, 1),
        jp_patch=JpPatch(),
    )
    resp = client.create_sak(sak_req)
    sak = resp.sak
    journalpost = resp.journalpost
    logger.info(f"Updated with sak. SakansvarligPersonId: {sak.SaksansvarligPersonId}")
    logger.info(f"Updated journalpost.Id : {journalpost.Id}")
    assert sak is not None
    assert sak.Id is not None
    assert sak.Id == sak_id
    print(f"Sak nr ===> {sak.Id}")

    assert_sak(tittel, off_tittel, sak, service_config, update=True)


@pytest.mark.integration
def test_search_with_escape(client, service_config):
    terms = {
        "Hansen, Ola - Tilsettingssak - Gjest ved HR-avdelingen" "",
        "It's a sin",
        "'It' many with '",
        'Test "hei',
        "Test 'hei",
        "En tittel som kan gi feilmelding pga første ord er ikke gyldig"
        "Ikke søkbar tittel",
    }

    for term in terms:
        # create new jp only when needed
        logger.info("TERM=>", term)
        # uncomment when first running to store jp so search returns
        # sak_req = CreateSakReq(
        #     sak_id=data["sak_id_" + service_config.interface],
        #     tittel=term,
        #     off_tittel=get_hide_name(term),
        #     person=person,
        #     dokument_beskrivelse=term,
        #     sek_klassering=get_sek_klassering(service_config),
        #     adm_enhet=data["adm_id_" + service_config.interface],
        #     dokument_dato=date(2020, 1, 1),
        #     jp_patch=JpPatch(),
        # )
        # resp = client.create_sak(sak_req)
        logger.info(f"Innholdsbeskrivelse: {term}")
        search_result = client.search_jp_by_field("Innholdsbeskrivelse", term)

        assert search_result is not None
        assert len(search_result) >= 1

        found = list(filter(lambda x: (term == x.Innholdsbeskrivelse), search_result))
        print(f"Found: {len(found)}")
        assert len(found) >= 0


@pytest.mark.integration
def test_create_sak_1(client, service_config: EphClientConfig, test_doc: Doc):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)

    req, resp = do_create_sak(
        client, off_tittel, service_config, tittel, test_doc=test_doc
    )

    act_sak = resp.sak
    act_journalpost = resp.journalpost
    act_avsender_mottaker = resp.avsender_mottaker

    assert_sak(tittel, off_tittel, act_sak, service_config)
    assert_journalpost(act_journalpost, act_sak, service_config, req)
    assert_avsender_mottaker(act_avsender_mottaker, service_config)

    assert_primaerklassering(act_sak, service_config)
    assert_sek_klassering(act_sak, service_config)
    assert_avskriv_restansen(client, act_journalpost.Id, service_config)
    logger.info(f"Created sak: {act_sak.Id}, journalpost: {act_journalpost.Id}")


@pytest.mark.integration
def test_fetch_sak(client, service_config):
    sak_id = data["sak_id_" + service_config.interface]
    sak = client.fetch_by_id("Sak", sak_id)

    # sak.SaksstatusId = "A"
    # client.insert_or_update(sak, update=True)
    # print(sak)
    assert sak
    assert sak.Id


@pytest.mark.integration
def test_search_jp_by_field(client, service_config):
    jp_list = client.search_jp_by_field(
        "tilleggsattributt1", "1"
    )  # data["sekvensnr_" + service_config.interface]

    # jp_list.SaksstatusId = "A"
    # client.insert_or_update(jp_list, update=True)
    print(len(jp_list))
    assert jp_list
    for jp in jp_list:
        assert jp.Id


@pytest.mark.integration
def test_search_created_sak(client, service_config):
    if is_not_uib(service_config):
        return
    saksaar = 2022
    sak_nr = 1239
    sak = client.search_created_sak(saksaar, sak_nr)
    print("saksId", sak.Id)
    assert sak
    assert sak.Id


@pytest.mark.integration
def test_search_created_sak_prinsipp(client, service_config):
    saksaar = 2022
    sekvensnummer = 1877
    if service_config.interface == "uio":
        sekvensnummer = 1
    created_sak = client.search_created_sak(saksaar, sekvensnummer)

    assert created_sak


@pytest.mark.integration
def test_sak_patch(client, service_config, test_doc):
    if is_not_uib(service_config):
        return

    sak_id = 251691
    saksaar = 2022
    sak_sekvensnr = 1877

    org_tittel = client.fetch_by_id("Sak", sak_id).Tittel

    from random import randint

    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)
    req = CreateSakReq(
        sak_id=sak_id,
        sekvensnummer=sak_sekvensnr,
        saksaar=saksaar,
        person=person,
        dokument_beskrivelse=jp_tittel,
        sek_klassering=get_sek_klassering(service_config),
        docs=[test_doc],
        adm_enhet=get_adm_enhet(service_config),
        dokument_dato=date(2017, 1, 1),
        jp_patch=JpPatch(
            tilleggsattributt1="23",
        ),
        tittel=tittel + "_" + str(randint(100, 10000)),
        off_tittel=off_tittel + "_" + str(randint(100, 10000)),
        sak_patch_fields={"Tittel", "TittelOffentlig"},
    )

    client.patch_sak(req)

    sak_patched = client.fetch_by_id("Sak", req.sak_id)

    assert org_tittel != sak_patched.Tittel


@pytest.mark.integration
def test_search_created_sak_not_found(client, service_config):
    saker = client.search_created_sak(1, 1)
    assert not saker


@pytest.mark.integration
def test_create_single_sak(client, service_config: EphClientConfig, test_doc: Doc):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)

    req, resp = do_create_sak(
        client, off_tittel, service_config, tittel=tittel, test_doc=test_doc
    )

    act_sak = resp.sak
    act_journalpost = resp.journalpost
    act_avsender_mottaker = resp.avsender_mottaker

    assert_sak(tittel, off_tittel, act_sak, service_config)
    assert_journalpost(act_journalpost, act_sak, service_config, req)
    assert_avsender_mottaker(act_avsender_mottaker, service_config)
    assert_primaerklassering(act_sak, service_config)
    assert_sek_klassering(act_sak, service_config)
    logger.info(f"Created sak: {act_sak.Id}, journalpost: {act_journalpost.Id}")


@pytest.mark.integration
def test_create_sak_w_saksbehandler(
    client, service_config: EphClientConfig, test_doc: Doc
):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)

    req, resp = do_create_sak(
        client, off_tittel, service_config, tittel, test_doc=test_doc
    )

    act_sak = resp.sak
    act_jp = resp.journalpost
    act_avsender_mottaker = resp.avsender_mottaker

    assert_sak(tittel, off_tittel, act_sak, service_config)
    assert_journalpost(act_jp, act_sak, service_config, req)
    assert_avsender_mottaker(act_avsender_mottaker, service_config)
    assert_primaerklassering(act_sak, service_config)
    assert_sek_klassering(act_sak, service_config)
    assert_saksbehandler(client, resp, service_config)


@pytest.mark.integration
def test_create_update_sak(client, service_config: EphClientConfig, test_doc: Doc):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)

    req = CreateSakReq(
        person=person,
        dokument_beskrivelse=jp_tittel,
        sek_klassering=get_sek_klassering(service_config),
        docs=[test_doc],
        adm_enhet=get_adm_enhet(service_config),
        dokument_dato=date(2017, 1, 1),
        jp_patch=JpPatch(
            tilleggsattributt1="23",
        ),
        tittel=tittel,
        off_tittel=off_tittel,
    )
    req, resp = do_create_sak(
        client, off_tittel, service_config, tittel, req, test_doc=test_doc
    )

    act_sak = resp.sak
    act_journalpost = resp.journalpost
    act_avsender_mottaker = resp.avsender_mottaker

    assert_sak(tittel, off_tittel, act_sak, service_config)
    assert_journalpost(act_journalpost, act_sak, service_config, req)
    assert_avsender_mottaker(act_avsender_mottaker, service_config)
    assert_primaerklassering(act_sak, service_config)
    assert_sek_klassering(act_sak, service_config)


@pytest.mark.integration
def test_get_sak(service_config: EphClientConfig):
    client = get_client(service_config)
    sak_id = 251117  # data["sak_id_" + service_config.interface]

    admin_id: int = data["adm_id_" + service_config.interface]  # type: ignore[assignment]
    client.fetch_by_id("AdministrativEnhet", admin_id)
    sak = client.fetch_by_id("sak", sak_id)
    sak = EphSak.from_orm(sak)
    # print(sak)
    assert sak is not None


@pytest.mark.integration
def test_get_klassering_by_sak_id(service_config: EphClientConfig):
    if service_config.interface != "uib":  # only used in uib
        return

    client = get_client(service_config)
    sak_id: int = data["sak_id_" + service_config.interface]  # type: ignore[assignment]
    klasseringer = client.get_klasseringer_by_sak_id(sak_id)
    print(klasseringer)
    assert klasseringer


@pytest.mark.integration
def test_search_wrong_emp_id(service_config: EphClientConfig):
    client = get_client(service_config)

    adm_id: int = data["adm_id_" + service_config.interface]  # type: ignore[assignment]
    resp = client.search_sak("Hansen", -1, adm_id)  # type: ignore[arg-type]
    assert resp.__len__() == 0


@pytest.mark.integration
def test_search_wrong_adm_enhet_id(service_config: EphClientConfig):
    client = get_client(service_config)

    resp = client.search_sak("Hansen", "102722", "-1")  # type: ignore[arg-type]
    assert resp.__len__() == 0


@pytest.mark.integration
def test_search_sak_with_title_emp_nr_and_adm_enhet_id(service_config: EphClientConfig):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)
    client = get_client(service_config)

    adm_id: int = data["adm_id_" + service_config.interface]  # type: ignore[assignment]
    resp = client.search_sak(
        tittel,
        emp_nr,
        adm_id,
    )
    prev_sak_id = None
    for sak in resp:
        if prev_sak_id:
            assert prev_sak_id > sak.Id
        prev_sak_id = sak.Id
    assert len(resp)


@pytest.mark.integration
def test_search_wrong_emp_nr_expect_none_hits(service_config: EphClientConfig):
    """uit does not use emp nr, the brith date is part of the sak tittel"""
    client = get_client(service_config)
    tittel = "Ola"
    if service_config.interface == "uit":
        tittel = "wrong_tittel"
    adm_id: int = data["adm_id_" + service_config.interface]  # type: ignore[assignment]
    resp = client.search_sak(tittel, 1, adm_id)  # type: ignore[arg-type]
    assert resp.__len__() == 0


@pytest.mark.integration
def test_search_wrong_name(service_config: EphClientConfig):
    client = get_client(service_config)

    adm_id: int = data["adm_id_" + service_config.interface]  # type: ignore[assignment]
    resp = client.search_sak(
        title="wrong name",
        emp_nr="-1",
        adm_enh_id=adm_id,
    )
    assert resp.__len__() == 0


@pytest.mark.integration
def test_search_with_comma(service_config: EphClientConfig):
    client = get_client(service_config)

    adm_id: int = data["adm_id_" + service_config.interface]  # type: ignore[assignment]
    resp = client.search_sak(
        title="wrong name",
        emp_nr="-1",
        adm_enh_id=adm_id,
    )
    assert resp.__len__() == 0


@pytest.mark.integration
def test_search_jp_has_valid_saksbehandler(service_config: EphClientConfig):
    """Helper test for finding saksbehandler person id"""
    client = get_client(service_config)
    username = data["api_user_" + service_config.interface]
    search_resp = client._do_search(f"Brukernavn={username}", "Person", EphPerson)

    assert search_resp
    person = search_resp[0]

    search_resp = client._do_search(
        f"PersonId={person.Id}", "PersonNavn", EphPersonNavn
    )
    person_navn_resp = search_resp[0]
    logger.info(f"username: {person.Brukernavn}, person_navn_id: {person_navn_resp.Id}")


@pytest.mark.integration
def test_avskriv_restansen(service_config: EphClientConfig, client, test_doc):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)

    # avskriving is done as part of create_sak
    req, resp = do_create_sak(
        client, off_tittel, service_config, tittel, test_doc=test_doc
    )

    assert_avskriv_restansen(client, resp.journalpost.Id, service_config)


# TODO: should this return any results?
@pytest.mark.integration
def test_get_jp_by_field_with_escape(service_config: EphClientConfig):
    client = get_client(service_config)
    field = get_jp_tillegg_attr(service_config)
    resp1 = client.search_jp_by_field(field, "-1")

    assert len(resp1) >= 0


@pytest.mark.integration
def test_get_jp_by_sekvensnr(service_config: EphClientConfig):
    client = get_client(service_config)
    field = get_jp_tillegg_attr(service_config)
    resp1 = client.search_jp_by_field(field, "1")

    assert len(resp1)


@pytest.mark.integration
def test_get_jp_by_field_date_from(service_config: EphClientConfig):
    tilleggs_attr_field = get_jp_tillegg_attr(service_config)
    client = get_client(service_config)
    date_from = date(2022, 6, 1)
    date_to = date(2022, 6, 21)
    fields: list[tuple[str, str]] = [
        (tilleggs_attr_field, "*"),
        (
            "TilgangskodeId",
            service_config.jp_config.tilgangskode_id,
        ),  # type: ignore[list-item]
        ("Innholdsbeskrivelse", "Signert arbeidsavtale*"),
    ]
    journalposts = client.search_jp_by_date(
        fields=fields,
        date_from=date_from,
        date_to=date_to,
    )
    logger.info("search hits: %s", len(journalposts))
    for jp in journalposts:
        logger.info(jp.Id)
        logger.info(getattr(jp, tilleggs_attr_field))
        logger.info(jp.Innholdsbeskrivelse)
        logger.info(jp.Endret)

        logger.info("==========================")

    assert len(journalposts) > 0
    for eph_jp in journalposts:
        assert int(getattr(eph_jp, tilleggs_attr_field)) > 0
        assert eph_jp.Hjemmel == service_config.jp_config.hjemmel
        assert eph_jp.TilgangskodeId == service_config.jp_config.tilgangskode_id
        assert "Signert arbeidsavtale".lower() in eph_jp.Innholdsbeskrivelse.lower()
        assert eph_jp.Opprettet.date() >= date_from
        assert eph_jp.Opprettet.date() <= date_to


@pytest.mark.integration
def test_search_save_response(service_config: EphClientConfig):
    sak_tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)
    client = get_client(service_config)
    adm_id: int = data["adm_id_" + service_config.interface]  # type: ignore[assignment]
    sak_resp = client.search_sak(
        sak_tittel,
        emp_nr,
        adm_id,
    )

    saker = [EphSak.from_orm(sak) for sak in sak_resp]

    def default(o):
        if isinstance(o, (date, datetime)):
            return o.isoformat()

    saker = saker[0:3]
    saker_dict = [sak.dict() for sak in saker]
    with open("tests/res/eph_search_sak_resp.json", "w") as f:
        f.write(json.dumps(saker_dict, default=default, sort_keys=True))


@pytest.mark.integration
def test_get_jp_by_field(service_config: EphClientConfig):
    client = get_client(service_config)
    resp1 = client.search_jp_by_field("tilleggsattributt1", "1")
    resp2 = client.search_jp_by_field("tilleggsattributt2", "2")
    resp3 = client.search_jp_by_field("tilleggsattributt3", "3")
    resp4 = client.search_jp_by_field("tilleggsattributt4", "4")
    resp5 = client.search_jp_by_field("tilleggsattributt5", "5")
    resp10 = client.search_jp_by_field("tilleggsattributt10", "10")

    assert_jp_by_field(resp1)
    assert_jp_by_field(resp2)
    assert_jp_by_field(resp3)
    assert_jp_by_field(resp4)
    assert_jp_by_field(resp5)
    assert_jp_by_field(resp10)


@pytest.mark.integration
def test_delete_klasseringer(service_config: EphClientConfig):
    client = get_client(service_config)

    klasseringer = client.get_klasseringer_by_sak_id("251054")

    for kl in klasseringer:
        if kl.OrdningsprinsippId == "ANR":
            kl = client.fetch_by_id("Klassering", kl.Id)
            kl.OrdningsverdiId = "000000"
            client.insert_or_update(ins_obj=[kl], update=True)
        print("klasseringer", klasseringer)


@pytest.mark.integration
def test_get_jp_by_field_set_status(service_config: EphClientConfig):
    """Get jp with given sekvensnr and update status (to avoid already saved case and save jp again)"""
    client = get_client(service_config)
    sekvensnr: str = data["sekvensnr_" + service_config.interface]  # type: ignore[assignment]
    resp = client.search_jp_by_field("tilleggsattributt1", sekvensnr)
    for r in resp:
        jp = client.fetch_by_id("Journalpost", r.Id)  # type: ignore[arg-type]
        sak = client.fetch_by_id("Sak", r.SakId)  # type: ignore[arg-type]
        # Don't set sak status due to testing pre-created-sak
        # sak.SaksstatusId = "A"
        jp.JournalstatusId = "U"
        jp.attr__Id = None
        sak.attr__Id = None
        updated = client.insert_or_update([jp, sak], update=True)
        print(f"Updated..: {sekvensnr}")
    print(f"resp: {resp}")


@pytest.mark.integration
def test_reset_klassering(service_config: EphClientConfig):
    if is_not_uib(service_config):
        return

    client = get_client(service_config)

    sak_id: int = data["sak_id_pre_created_" + service_config.interface]  # type: ignore[assignment]
    sak = client.fetch_by_id("Sak", sak_id)
    klasseringer = client.get_klasseringer_by_sak_id(sak.Id)

    print("klasseringer: ", klasseringer)
    klasser = []
    for klassering in klasseringer:
        klass = client.fetch_by_id("Klassering", klassering.Id)
        if klass.OrdningsprinsippId != "ARKNOK":
            klass.OrdningsverdiId = "000000"
            klass.attr__Id = None
            klasser.append(klass)

    updated = client.insert_or_update(klasser, update=True)
    print("updated", updated)
    assert updated


@pytest.mark.integration
def test_sak_status(service_config: EphClientConfig):
    client = get_client(service_config)
    sak_id: int = data["sak_id_" + service_config.interface]  # type: ignore[assignment]
    sak = client.fetch_by_id("Sak", sak_id)
    status_under_behandling = "B"
    sak.SaksstatusId = status_under_behandling
    updated = client.insert_or_update([sak], update=True)
    assert updated[0].SaksstatusId == status_under_behandling


@pytest.mark.integration
def test_search_sak(service_config: EphClientConfig):
    if is_not_uib(service_config):
        return
    client = get_client(service_config)
    sak = client.search_created_sak(2022, 1877)

    sak = client.fetch_by_id("Sak", sak.Id)  # type: ignore[arg-type]
    sak.TilgangskodeId = "P"
    sak.attr__Id = None

    client.insert_or_update([sak], update=True)
    assert sak


@pytest.mark.integration
def test_create_sak_with_pre_created_sak(service_config: EphClientConfig, test_doc):
    if is_not_uib(service_config):
        return
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)
    sak_id = data["sak_id_" + service_config.interface]
    test_doc.name = (
        "Signert arbeidsavtale (Timekontrakt) - Ukjent org(10000083) - "
        "05.06.2021-22.12.2021.pdf"
    )
    sak_req = CreateSakReq(
        sak_id=sak_id,
        tittel=tittel,
        off_tittel=off_tittel,
        person=person,
        dokument_beskrivelse=jp_tittel,
        sek_klassering=get_sek_klassering(service_config),
        docs=[test_doc],
        adm_enhet=get_adm_enhet(service_config),
        dokument_dato=date(2020, 1, 1),
        jp_patch=JpPatch(),
    )

    client = get_client(service_config)
    # search pre created sak
    sak = client.search_created_sak(2022, 1238)
    sak_req.sak_id = sak.Id
    sak_req.sak_patch_fields = {
        "Tittel",
        "TittelOffentlig",
        "Hjemmel",
        "TilgangskodeId",
        "ArkivdelId",
    }

    sak_resp = client.create_sak(sak_req)
    assert sak_resp


@pytest.mark.integration
def test_delete_klassering_by_sak_id(service_config: EphClientConfig, test_doc: Doc):
    client = get_client(service_config)
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)
    req, resp = do_create_sak(
        client, off_tittel, service_config, tittel, test_doc=test_doc
    )

    act_sak = resp.sak

    assert act_sak.Primaerklassering
    assert act_sak.Sekundaerklassering

    client.delete_klasseringer(act_sak.Id)

    klassering = client.get_klasseringer_by_sak_id(act_sak.Id)
    assert not klassering


@pytest.mark.integration
def test_saksbehandler(service_config: EphClientConfig):
    client = get_client(service_config)
    sak_id = data["sak_id_" + service_config.interface]
    jp_id = data["jp_id_" + service_config.interface]
    sak = client.fetch_by_id("Sak", sak_id)  # type: ignore[arg-type]
    jp = client.fetch_by_id("Journalpost", jp_id)  # type: ignore[arg-type]

    sak.SaksansvarligPersonId = 0
    jp.SaksbehandlerId = 0

    sak.attr__Id = None
    jp.attr__Id = None
    client.insert_or_update(ins_obj=[sak, jp], update=True)

    sak = client.fetch_by_id("Sak", sak_id)  # type: ignore[arg-type]
    jp = client.fetch_by_id("Journalpost", jp_id)  # type: ignore[arg-type]

    logger.info(f"sak.sakansvarligPersonId: {sak.SaksansvarligPersonId}")
    logger.info(f"jp.saksbehandlerId: {jp.SaksbehandlerId}")
    assert sak.SaksansvarligPersonId == client.get_saksbehandler_pn_id()
    assert jp.SaksbehandlerId == client.get_saksbehandler_pn_id()


@pytest.mark.integration
def test_create_sak_multiple_documents(
    client, service_config: EphClientConfig, test_doc: Doc
):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)

    test_doc_2 = deepcopy(test_doc)
    test_doc_2.name = "simple_2.pdf"
    test_doc_2.title = "another simple pdf file"

    req = CreateSakReq(
        person=person,
        dokument_beskrivelse=jp_tittel,
        sek_klassering=get_sek_klassering(service_config),
        docs=[test_doc, test_doc_2],
        adm_enhet=get_adm_enhet(service_config),
        dokument_dato=date(2017, 1, 1),
        jp_patch=JpPatch(
            tilleggsattributt1="23",
            status="S",
        ),
        tittel=tittel,
        off_tittel=off_tittel,
    )
    req, resp = do_create_sak(client, off_tittel, service_config, tittel, req)

    act_sak = resp.sak
    act_journalpost = resp.journalpost
    act_avsender_mottaker = resp.avsender_mottaker

    assert_sak(tittel, off_tittel, act_sak, service_config)
    assert_journalpost(act_journalpost, act_sak, service_config, req)
    assert_avsender_mottaker(act_avsender_mottaker, service_config)
    assert_primaerklassering(act_sak, service_config)
    assert_sek_klassering(act_sak, service_config)


@pytest.mark.integration
def test_create_sak_multiple_avs_mot(
    client, service_config: EphClientConfig, test_doc: Doc
):
    tittel, off_tittel = get_sak_tittel(service_config, person, kontrakt_type)

    req = CreateSakReq(
        person=person,
        dokument_beskrivelse=jp_tittel,
        sek_klassering=get_sek_klassering(service_config),
        docs=[test_doc],
        adm_enhet=get_adm_enhet(service_config),
        dokument_dato=date(2017, 1, 1),
        jp_patch=JpPatch(
            tilleggsattributt1="23",
            status="S",
        ),
        tittel=tittel,
        off_tittel=off_tittel,
        avs_mottaker_patch=[
            AvsenderMottakerPatch(
                AdministrativEnhetId=1,
                Innholdstype=True,
                KopimottakerMedavsender=True,
            ),
            AvsenderMottakerPatch(
                AdministrativEnhetId=2,
                Innholdstype=True,
                KopimottakerMedavsender=True,
            ),
        ],
    )
    req, resp = do_create_sak(client, off_tittel, service_config, tittel, req)

    act_sak = resp.sak
    act_journalpost = resp.journalpost
    act_avsender_mottaker = resp.avsender_mottaker

    assert_sak(tittel, off_tittel, act_sak, service_config)
    assert_journalpost(act_journalpost, act_sak, service_config, req)
    assert_avsender_mottaker(act_avsender_mottaker, service_config)
    assert_primaerklassering(act_sak, service_config)
    assert_sek_klassering(act_sak, service_config)


def get_jp_tillegg_attr(service_config):
    """field used to store sekvensnr i jp (config in toa-ms)"""
    field = "Tilleggsattributt1"
    if service_config.interface == "uit":
        field = "Tilleggsattributt10"
    return field


def do_create_sak(client, off_tittel, service_config, tittel, req=None, test_doc=None):
    if not req:
        req = CreateSakReq(
            person=person,
            dokument_beskrivelse=jp_tittel,
            sek_klassering=get_sek_klassering(service_config),
            docs=[test_doc],
            adm_enhet=get_adm_enhet(service_config),
            dokument_dato=date(2017, 1, 1),
            jp_patch=JpPatch(
                tilleggsattributt1="1",
                tilleggsattributt2="2",
                tilleggsattributt3="3",
                tilleggsattributt4="4",
                tilleggsattributt5="5",
                tilleggsattributt10="10",
            ),
            tittel=tittel,
            off_tittel=off_tittel,
        )
    resp = client.create_sak(req)
    return req, resp


def assert_jp_by_field(resp):
    assert resp is not None
    assert len(resp) > 0
    assert isinstance(resp[0], EphJournalpost)


def assert_avsender_mottaker(avs_mot: EphAvsenderMottaker, service_config):
    assert avs_mot is not None
    assert avs_mot.Navn == person.short_name()
    assert avs_mot.EPostAdresse == person.email
    assert avs_mot.Postnummer == person.post_code
    assert avs_mot.Postadresse == person.post_address
    assert avs_mot.Poststed == person.post_place
    assert avs_mot.UntattOffentlighet == service_config.jp_config.avsender_untatt_off


def assert_sak(
    exp_tittel: str,
    exp_off_tittel: str,
    sak,
    service_config: EphClientConfig,
    update: bool = False,
):
    logger.info(f"sak_id: {sak.Id}")
    adm_enhet = get_adm_enhet(service_config)
    assert sak is not None
    assert sak.Id is not None
    assert sak.Tittel == exp_tittel
    assert sak.TittelPersonnavn == exp_off_tittel.strip()
    assert sak.TittelOffentlig == exp_off_tittel
    assert sak.ArkivdelId == service_config.sak_config.arkivdel_id
    assert sak.Hjemmel == service_config.sak_config.hjemmel
    assert sak.JournalEnhetId == adm_enhet.journal_enhet_id
    assert sak.SaksstatusId == service_config.sak_config.status_id
    assert sak.TilgangskodeId == service_config.sak_config.tilgangskode_id
    assert sak.TilgangsgruppeId == service_config.sak_config.tilgangsgruppe_id

    if not update:  # on update sak does not have AnsvarligEnhet fetched
        assert sak.AnsvarligEnhet.Id == adm_enhet.id
        assert sak.AnsvarligEnhet.JournalEnhetId == adm_enhet.journal_enhet_id


def assert_journalpost(
    journalpost: EphJournalpost,
    sak: EphSak,
    service_config: EphClientConfig,
    req: CreateSakReq,
):
    logger.info(f"journal post id: {journalpost.Id}")
    adm_enhet = get_adm_enhet(service_config)
    assert journalpost is not None
    assert journalpost.Id is not None
    assert journalpost.ArkivdelId == service_config.jp_config.arkivdel_id
    assert journalpost.DokumenttypeId == service_config.jp_config.dokument_type_id
    assert journalpost.Hjemmel == service_config.jp_config.hjemmel
    assert (
        journalpost.DokumentkategoriId == service_config.jp_config.dokumentkategori_id
    )
    assert journalpost.Innholdsbeskrivelse == jp_tittel
    assert journalpost.JournalEnhetId == adm_enhet.journal_enhet_id
    assert journalpost.SakId == sak.Id
    assert journalpost.AvsenderMottaker == person.first_name
    assert journalpost.TilgangskodeId == service_config.jp_config.tilgangskode_id
    assert journalpost.Dokumentdato is not None
    assert journalpost.Dokumentdato == req.dokument_dato
    assert journalpost.Tilleggsattributt1 == req.jp_patch.tilleggsattributt1
    assert journalpost.Tilleggsattributt2 == req.jp_patch.tilleggsattributt2
    assert journalpost.Tilleggsattributt3 == req.jp_patch.tilleggsattributt3
    assert journalpost.Tilleggsattributt4 == req.jp_patch.tilleggsattributt4
    assert journalpost.Tilleggsattributt5 == req.jp_patch.tilleggsattributt5
    assert journalpost.Tilleggsattributt10 == req.jp_patch.tilleggsattributt10


def assert_saksbehandler(client, resp, service_config):
    act_sak = client.fetch_by_id("Sak", resp.sak.Id)
    act_jp = client.fetch_by_id("Journalpost", resp.journalpost.Id)

    assert (
        act_jp.SaksbehandlerId == client.get_saksbehandler_pn_id()
    ), "jp.saksbehandlerId should be same"

    assert (
        act_sak.SaksansvarligPersonId == client.get_saksbehandler_pn_id()
    ), "sak.SaksansvarligPersonId should be same"


def assert_sek_klassering(act_sak, service_config):
    """Assert sekundær klassering, uit does not have any"""
    if service_config.interface == "uit":
        return
    sek_prinsipp_id = get_sek_klassering(service_config).ordnings_verdi_id
    sek_prinsipp_value = get_sek_klassering(service_config).beskrivelse
    if "uio" in service_config.interface:
        sek_prinsipp_id = person.emp_nr
        sek_prinsipp_value = person.name()

    assert_klassering(
        act_sak.Sekundaerklassering,
        service_config.sak_config.sek_ordningsprinsipp,
        sek_prinsipp_id,
        sek_prinsipp_value,
        service_config.sak_config.sek_klassering_untatt_off,
    )


def assert_primaerklassering(act_sak, service_config):
    prim_prinsipp_id = person.emp_nr
    prim_prinsipp_value = person.name()
    if "uio" in service_config.interface:
        prim_prinsipp_id = get_sek_klassering(service_config).ordnings_verdi_id
        prim_prinsipp_value = get_sek_klassering(service_config).beskrivelse
    elif "uit" in service_config.interface:
        prim_prinsipp_id = service_config.sak_config.primaer_ordningsprinsipp_id
        prim_prinsipp_value = service_config.sak_config.primaer_ordningsprinsipp_value

    assert_klassering(
        act_sak.Primaerklassering,
        service_config.sak_config.primaer_ordningsprinsipp,
        prim_prinsipp_id,
        prim_prinsipp_value,
        service_config.sak_config.primaer_klassering_untatt_off,
    )


def assert_klassering(
    klassering: EphKlassering,
    ordningsprinsipp_id: str,
    ordningsprinsipp_verdi_id: str,
    beskrivelse: str,
    untatt_off: bool,
):
    assert klassering is not None
    assert klassering.OrdningsprinsippId == ordningsprinsipp_id
    assert klassering.OrdningsverdiId == ordningsprinsipp_verdi_id
    assert klassering.Beskrivelse == beskrivelse
    assert klassering.UntattOffentlighet == untatt_off


def assert_avskriv_restansen(client, jp_id, service_config):
    resp = client._do_search(
        f"JournalpostId={jp_id}",
        "AvsenderMottaker",
        EphAvsenderMottaker,
    )
    assert resp
    assert resp[0].AvskrivningsmaateId == service_config.jp_config.avskriving_maate


def get_adm_enhet(service_config):
    return EphAdministrativEnhet(
        id=data["adm_id_" + service_config.interface],
        betegnelse="HR-avdelingen",
        kortnavn="HR",
        journal_enhet_id=data["je_id_" + service_config.interface],
    )


def get_sek_klassering(service_config):
    """UiT does not use sek klassering"""
    if service_config.interface == "uit":
        return None
    return Sek_klassering(
        ordnings_verdi_id=data["sek_kl_verdi_" + service_config.interface],
        beskrivelse=data["sek_kl_besk_" + service_config.interface],
    )


def _get_config_value(service_config, conf_name, def_value):
    return data.get(f"{conf_name}_{service_config.interface}", def_value)


SAK_TITTEL_MAL_UIT = "Personalmappe - %s - %s"  # Birth date (ddMMYY)


def get_sak_tittel(eph_config: EphClientConfig, person: Person, kontrakt_type: str):
    if eph_config.interface == "uit":
        name = "%s %s" % (person.first_name, person.last_name)

        sak_tittel = SAK_TITTEL_MAL_UIT % (name, person.birth_date)
        off_tittel = SAK_TITTEL_MAL_UIT % (
            get_hide_name(name),
            get_hide_name(person.birth_date),
        )
    else:
        name = f"{person.last_name}, {person.first_name}"
        hide_name = get_hide_name(name)
        sak_tittel = f"{name} - {kontrakt_type}"
        off_tittel = f"{hide_name} - {kontrakt_type}"

    logger.info("Got sak.tittel: %s", sak_tittel)
    return sak_tittel, off_tittel


def get_hide_name(name):
    return " ".join(["*****"] * len(name.split(" ")))


def is_not_uib(service_config):
    return not service_config.interface == "uib"
