import collections.abc
import json
import re
from datetime import date, datetime
from enum import Enum
from typing import Any, Literal, TypeVar

import pydantic
from pydantic import validator

BaseModel_T = TypeVar("BaseModel_T", bound="BaseModel")


def to_date(dtext: str) -> date:
    match = re.search(r"\d{2}.\d{2}.\d{4}", dtext)
    if match is None:
        raise ValueError("Failed to match date")
    dt = datetime.strptime(match.group(), "%d.%m.%Y").date()
    return dt


def to_lower_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8"""
    first, *others = s.split("_")
    return "".join([first.lower(), *map(str.capitalize, others)])


def to_upper_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8"""
    parts = s.split("_")
    return "".join(map(str.capitalize, parts))


def decapitalize_first_char(s: str) -> str:
    f = s[0]
    r = s[1:]
    return "".join([f.lower(), r])


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls: type[BaseModel_T], data: dict[str, Any]) -> BaseModel_T:
        return cls(**data)

    @classmethod
    def from_json(cls: type[BaseModel_T], json_data: str) -> BaseModel_T:
        data = json.loads(json_data)
        return cls.from_dict(data)

    def dict(
        self,
        by_alias: bool = True,
        exclude_unset: bool = True,
        *args: Any,
        **kwargs: Any,
    ) -> dict[Any, Any]:
        return super().dict(
            by_alias=by_alias, exclude_unset=exclude_unset, *args, **kwargs
        )

    def keys(self) -> collections.abc.KeysView[str]:
        return self.__fields__.keys()

    def __getitem__(self, item: str) -> Any:
        return self.__getattribute__(item)


class EphUploadHeader(BaseModel):
    ephorte_identity: Any
    file_name: str
    storage_identifier: str

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class EphDocumentContentHeader(BaseModel):
    document_id: int
    identity: Any
    variant: str
    version: int

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class EphSoapCredentials(BaseModel):
    database: str
    user_name: str
    password: str
    external_system_name: str

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class EphSoapService(BaseModel):
    wsdl: pydantic.HttpUrl
    headers: dict[str, str]
    identity: EphSoapCredentials
    alternate_endpoint: pydantic.HttpUrl | None = None


class UploadMessage(BaseModel):
    Content: Any


class Search(BaseModel):
    title: str | None = None
    adm_enh_id: int
    status_avsluttet: str
    status_utgaat: str
    prim_ord_prinsipp: str | None = None
    sek_ord_prinsipp: str | None = None
    ord_verdi_id: str | None = None

    # Dont need to check for escaping, waiting for response from Sikri
    # @root_validator()
    # def is_valid_content(cls, values: Dict) -> Dict:
    #    for field in values.keys():
    #        val = values.get(field)
    #        match = re.match(r"^[-.,()\s\w]+$", val)
    #        if match is None:
    #            raise ValueError(f"Search failed, not accepted search term: {val}")
    #    return values


class EphSearchConfig(BaseModel):
    arkivdel_id: str
    status_avsluttet: str
    status_utgaat: str


class EphDokumentType(BaseModel):
    ut_gaaende: str
    inn_gaaende: str


class EphDocConfig(BaseModel):
    dokument_kategori_id: str
    format_pdf: str
    format_html: str
    format_txt: str
    format_tiff: str
    tilgangs_kode_id: str
    tilknyttning_hoved: str
    tilknyttning_vedlegg: str


PrinsippName = Literal["primær", "sekundær"]


class PrinsippType(str, Enum):
    """primær og sek ordningsprinsipp typer i ephorte"""

    arknok = "ARKNOK"
    ansatt_nr = "ANSATTNR"
    anr = "ANR"
    arkuit = "ARKUIT"
    emne = "EMNE"  # Arkivnøkkel for NTNU
    fdato = "FDATO"  # Birthdate of employee (six digits)
    fnra = "FNRA"


class Saksbehandler(BaseModel):
    person_navn_id: int


class EphSakConfig(BaseModel):
    arkivdel_id: str
    hjemmel: str | None = None
    primaer_ordningsprinsipp: str
    primaer_ordningsprinsipp_id: str | None = None
    primaer_ordningsprinsipp_value: str | None = None
    sek_ordningsprinsipp: str | None = None
    status_id: str
    tilgangskode_id: str | None = None
    tilgangsgruppe_id: int | None = None
    primaer_klassering_untatt_off: bool
    sek_klassering_untatt_off: bool


class SakPatch(EphSakConfig):
    """Overrides values in EphSakConfig"""

    arkivdel_id: str | None = None  # type: ignore[assignment]
    primaer_ordningsprinsipp: str | None = None  # type: ignore[assignment]
    status_id: str | None = None  # type: ignore[assignment]
    primaer_klassering_untatt_off: bool | None = None  # type: ignore[assignment]
    sek_klassering_untatt_off: bool | None = None  # type: ignore[assignment]


class EphJpConfig(BaseModel):
    dokument_type_id: str
    dokumentkategori_id: str | None = None
    hjemmel: str
    status: str
    arkivdel_id: str | None = None
    tilgangskode_id: str | None = None
    avsender_untatt_off: bool
    set_saksbehandler_id_on_avsmot: bool = False
    avskriving_maate: str | None = None
    tilleggsattributt1: str | None = None
    tilleggsattributt2: str | None = None
    tilleggsattributt3: str | None = None
    tilleggsattributt4: str | None = None
    tilleggsattributt5: str | None = None
    tilleggsattributt10: str | None = None


class CacheWsdl(BaseModel):
    enabled: bool = True
    path: str
    timeout: int = 60  # Default one hour cache


class EphClientConfig(BaseModel):
    interface: str  # uio|uib|ntnu|uio
    username: str | None = None
    def_saksbehandler_pn_id: int | None = None
    cache_wsdl: CacheWsdl | None = None
    document_service: EphSoapService | None = None
    object_service: EphSoapService | None = None
    function_service: EphSoapService | None = None
    search_config: EphSearchConfig | None = None
    sak_config: EphSakConfig | None = None
    jp_config: EphJpConfig | None = None
    doc_config: EphDocConfig | None = None
    dokument_type: EphDokumentType | None = None
    use_pre_created_sak: bool = False


class EphModel(BaseModel):
    class Config:
        orm_mode = True


class AktivtNavn(EphModel):
    Id: int


class PersonNavnID(EphModel):
    AktivtNavn: AktivtNavn


KlasseringPrinsippName = Literal["Sekundaerklassering", "Primaerklassering"]


class EphKlassering(EphModel):
    Id: int | None = None
    Beskrivelse: str | None = None
    Merknad: str | None = None
    OrdningsprinsippId: str | None = None
    OrdningsverdiId: str | None = None
    UntattOffentlighet: bool | None = None
    Sak: Any | None = None
    Journalpost: Any | None = None
    SakId: int | None = None


EphKlassering.update_forward_refs()


class EphJournalpost(EphModel):
    AdministrativEnhetId: int | None = None
    ArkivdelId: str | None = None
    AvsenderMottaker: str | None = None
    AvsenderMottakerUntattOffentlighet: bool | None = None
    DokumenttypeId: str | None = None
    DokumentkategoriId: str | None = None
    Dokumentdato: date | None = None
    Hjemmel: str | None = None
    Id: int | None = None
    Innholdsbeskrivelse: str | None = None
    InnholdsbeskrivelseOffentlig: str | None = None
    InnholdsbeskrivelsePersonnavn: str | None = None
    Hoveddokumenttype: str | None = None
    JournalEnhetId: str | None = None
    JournalstatusId: str | None = None
    Publisert: bool | None = None
    Sak: Any | None = None
    SakId: int | None = None
    TilgangsgruppeId: int | None = None
    TilgangskodeId: str | None = None
    UnntattOffentlighet: bool | None = None
    Primaerklassering: Any | None = None
    Sekundaerklassering: Any | None = None
    SaksbehandlerId: int | None = None
    Tilleggsattributt1: str | None = None
    Tilleggsattributt2: str | None = None
    Tilleggsattributt3: str | None = None
    Tilleggsattributt4: str | None = None
    Tilleggsattributt5: str | None = None
    Tilleggsattributt10: str | None = None
    Endret: datetime | None = None
    Opprettet: datetime
    attr__Id: str


class EphAvsenderMottaker(EphModel):
    Id: int | None = None
    Behandlingsansvarlig: Any | None = None
    Journalpost: Any | None = None
    JournalpostId: int | None = None
    Navn: str
    Person: bool | None = None
    HarRestanse: str | None = None
    AvskrivningsmaateId: str | None = None
    UntattOffentlighet: bool | None = None
    Postadresse: str | None = None
    Poststed: str | None = None
    Postnummer: str | None = None
    EPostAdresse: str | None = None
    VarsleMedEPost: bool | None = None
    AdministrativEnhetId: int | None = None
    JournalEnhetId: str | None = None
    Innholdstype: bool | None = None
    KopimottakerMedavsender: bool | None = None
    SaksbehandlerId: int | None = None


class AvsenderMottakerPatch(EphAvsenderMottaker):
    """Used to supply extra fields on creation of EphAvsenderMottaker"""

    Navn: str | None = None  # type: ignore[assignment]


class EphDokumentbeskrivelse(EphModel):
    Dokumenttittel: str
    DokumentkategoriId: str
    Papirdokument: bool
    TilgangskodeId: str
    attr__Id: str


class EphDokumentreferanse(EphModel):
    Dokumentbeskrivelse: Any | None = None
    Journalpost: Any | None = None
    TilknytningskodeId: str


class EphDokumentversjon(EphModel):
    Dokumentbeskrivelse: Any | None = None
    Dokumentreferanse: Any | None = None
    LagringsformatId: str
    TilgangskodeId: str | None = None


class JpPatch(EphJpConfig):
    """Used to override jp config values"""

    avsender_untatt_off: bool | None = None  # type: ignore[assignment]
    dokument_type_id: str | None = None  # type: ignore[assignment]
    hjemmel: str | None = None  # type: ignore[assignment]
    status: str | None = None  # type: ignore[assignment]


class EphEnhetstype(BaseModel):
    betegnelse: str | None = None
    id: str | None = None
    ledetekst: str | None = None
    underlagt_enhetstype: "EphEnhetstype | None" = None
    underlagt_enhetstype_id: str | None = None

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True
        orm_mode = True


EphEnhetstype.update_forward_refs()


class EphJournalstatus(BaseModel):
    benyttes_av_arkivet: bool | None = None
    benyttes_av_leder: bool | None = None
    benyttes_av_saksbehandler: bool | None = None
    betegnelse: str | None = None
    dokumentkontroll: str | None = None
    for_eksternt_produserte_dokument: bool | None = None
    for_internt_produserte_dokument: bool | None = None
    id: str | None = None
    kontroll_av_ekspedering: bool | None = None
    registreringsansvar: str | None = None
    hoved_id: int | None = None
    kortkode: str | None = None
    standard: bool | None = None

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True
        orm_mode = True


class EphJournalEnhet(BaseModel):
    avsluttet: Any | None = None
    betegnelse: str | None = None
    id: str | None = None
    lokalisering: str | None = None
    publisert: bool | None = None
    rapportgruppe: str | None = None
    standard_journalstatus_arkivpersonale1: EphJournalstatus | None = None
    standard_journalstatus_arkivpersonale1Id: str | None = None
    standard_journalstatus_arkivpersonale2: EphJournalstatus | None = None
    standard_journalstatus_arkivpersonale2Id: str | None = None
    standard_journalstatus_arkivpersonale3: EphJournalstatus | None = None
    standard_journalstatus_arkivpersonale3Id: str | None = None
    standard_journalstatus_saksbehandler1: EphJournalstatus | None = None
    standard_journalstatus_saksbehandler1Id: str | None = None
    standard_journalstatus_saksbehandler2: EphJournalstatus | None = None
    standard_journalstatus_saksbehandler2Id: str | None = None
    standard_journalstatus_saksbehandler3: EphJournalstatus | None = None
    standard_journalstatus_saksbehandler3Id: str | None = None
    standard_journalstatus_arkivpersonale1Hoved_id: int | None = None
    standard_journalstatus_arkivpersonale2Hoved_id: int | None = None
    standard_journalstatus_arkivpersonale3Hoved_id: int | None = None
    standard_journalstatus_saksbehandler1Hoved_id: int | None = None
    standard_journalstatus_saksbehandler2Hoved_id: int | None = None
    standard_journalstatus_saksbehandler3Hoved_id: int | None = None

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True
        orm_mode = True


class EphAdministrativEnhet(BaseModel):
    betegnelse: str | None = None
    enhetsforkortelse_dette_nivaa: str | None = None
    enhetstype: EphEnhetstype | None = None
    enhetstype_id: str | None = None
    fullstendig_sted: str | None = None
    id: int | None = None
    journal_enhet: EphJournalEnhet | None = None
    journal_enhet_id: str | None = None
    kortnavn: str | None = None
    nedlagt_dato: Any | None = None
    opprettet_dato: Any | None = None
    overordnet_enhet: "EphAdministrativEnhet | None" = None
    overordnet_enhet_id: int | None = None
    rapportgruppe: str | None = None

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True
        orm_mode = True


EphAdministrativEnhet.update_forward_refs()


class EphSak(EphModel):
    Id: int | None = None
    AnsvarligEnhet: Any | None = None
    ArkivdelId: str | None = None
    Hjemmel: str | None = None
    JournalEnhetId: str | None = None
    MappetypeId: str | None = None
    Primaerklassering: Any | None = None
    Sekundaerklassering: Any | None = None
    Tittel: str | None = None
    TittelPersonnavn: str | None = None
    TittelOffentlig: str | None = None
    TilgangsgruppeId: int | None = None
    TilgangskodeId: str | None = None
    SaksstatusId: str | None = None
    SaksansvarligPersonId: int | None = None
    Saksaar: int | None = None
    Sekvensnummer: int | None = None
    Opprettet: datetime | None = None

    attr__Id: str | None = None


EphSak.update_forward_refs()


class EphPerson(EphModel):
    Id: str
    Brukernavn: str


class EphPersonNavn(EphModel):
    Id: str
    PersonId: int
    Fornavn: str | None = None
    EtterNavn: str | None = None
    Aktiv: bool


class Person(BaseModel):
    first_name: str
    last_name: str
    emp_nr: str
    email_notify: bool = False
    birth_date: str
    fnr: str | None = None
    email: str | None = None
    post_address: str | None = None
    post_code: str | None = None
    post_place: str | None = None

    # adjust diff interface
    def short_name(self) -> str:
        return "{} {}".format(self.first_name, self.last_name)

    def name(self, delimiter: str | None = None) -> str:
        if delimiter == ",":
            return "{}, {}".format(self.last_name, self.first_name)
        return "{} {}".format(self.first_name, self.last_name)


class Doc(BaseModel):
    name: str
    content: bytes
    title: str
    format: str | None = None


class Sek_klassering(BaseModel):
    ordnings_verdi_id: str
    beskrivelse: str


class CreateSakReq(BaseModel):
    sak_id: int | None = None
    dokument_beskrivelse: str  # journal_post_innhold
    person: Person
    adm_enhet: EphAdministrativEnhet
    sek_klassering: Sek_klassering | None = None
    tittel: str
    off_tittel: str
    saksaar: int | None = None
    sekvensnummer: int | None = None  # Ephorte sak sekvensnummer
    docs: list[Doc] = []
    dokument_dato: date
    jp_patch: JpPatch
    sak_patch: SakPatch | None = None
    sak_patch_fields: set[str] | None = None
    avsender_mottaker_patch: list[AvsenderMottakerPatch] | None = None

    def apply_def_config(
        self,
        sub_config: EphJpConfig | EphSakConfig,
        patch_config_name: str,
    ) -> None:
        """Use specified sub config as default, overridden by specified patch object"""
        patch_config: JpPatch | SakPatch | None = getattr(self, patch_config_name)
        if patch_config is None:
            setattr(self, patch_config_name, sub_config)
            return

        fields_updated: dict[str, bool | str | None] = dict()
        for field in patch_config.keys():
            fields_updated[field] = self.config_as_def(sub_config, patch_config, field)

        setattr(self, patch_config_name, patch_config.from_dict(fields_updated))

    def config_as_def(
        self,
        sub_config: EphJpConfig | EphSakConfig,
        patch_config: JpPatch | SakPatch,
        field: str,
    ) -> bool | str | None:
        val: bool | str | None = (
            patch_config[field]
            if patch_config[field] is not None
            else sub_config[field]
        )
        return val

    @validator("sak_patch_fields")
    def sak_patch_only_when_sak_referanse(
        cls,
        v: dict[str, bool | str | None],
        values: dict[str, int | str | datetime | Any | None],
        **kwargs: Any,
    ) -> dict[str, bool | str | None] | None:
        if not v:
            if values["saksaar"] or values["sekvensnummer"]:
                raise ValueError("Missing sak_patch_field, saksaar or sekvensnummer")
            return None
        if values["saksaar"] is None or values["sekvensnummer"] is None:
            raise ValueError(
                f"saksaar: <{values['saksaar']}> or sekvensnummer: <{values['sekvensnummer']}> is missing"
            )
        valid_fields = EphSak.__fields__.keys()
        for field in v:
            if field not in valid_fields:
                raise ValueError(f"Invalid field: {field} in sak_patch_fields")

        return v


class CreateSakResp(BaseModel):
    sak: EphSak
    journalpost: EphJournalpost
    avsender_mottaker: EphAvsenderMottaker
