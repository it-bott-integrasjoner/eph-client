import functools
import logging
from datetime import date, datetime, timedelta
from mimetypes import guess_type
from typing import Any

import requests
import zeep
import zeep.client
import zeep.exceptions
import zeep.proxy
import zeep.transports
import zeep.xsd
from zeep.cache import SqliteCache

from .models import (
    AvsenderMottakerPatch,
    CacheWsdl,
    CreateSakReq,
    CreateSakResp,
    Doc,
    EphAdministrativEnhet,
    EphAvsenderMottaker,
    EphClientConfig,
    EphDocumentContentHeader,
    EphDokumentbeskrivelse,
    EphDokumentreferanse,
    EphDokumentversjon,
    EphJournalpost,
    EphKlassering,
    EphModel,
    EphSak,
    EphSoapService,
    EphUploadHeader,
    KlasseringPrinsippName,
    PersonNavnID,
    PrinsippName,
    PrinsippType,
    Search,
)

logger = logging.getLogger(__name__)
DEF_JP_SAKSBEHANDLER = 0  # sak.saksbehandler is then used
# Error msg from EIS, regarding saksbehandler not belonging to the right eph adm enhet
ERR_MSG_SAKSBHEANDLER = (
    "Valgt saksbehandler har ingen registert tilhørighet til valgt administrativ enhet"
)

# fmt: off
# Invalid words, when the search starts with following first words
INV_FIRST_SEARCH_WORDS = {
    "alle", "ang", "ang.", "angående", "at", "av", "brev", "den", "denne", "det", "dette",
    "ei", "en", "er", "for", "fra", "ha", "ham", "han", "hos", "hun", "hva", "hvem", "hvilke", "hvilken", "hvor", "i",
    "igjen", "igjennom", "ikke", "jeg", "kan", "kanskje", "med", "notat", "nå", "og", "også", "om", "oss", "pga",
    "pga.", "på", "sak", "saker", "si", "skal", "som", "til", "ved", "vedr", "vi"
}
# fmt: on


class ClientError(Exception):
    """ClientError: raised on wrong config of api or failed communications"""

    pass


class EphClient:
    """Ephorte client uses EiS to archive and search for ephorte sak"""

    def __init__(self, config: EphClientConfig, init_soap: bool = True) -> None:
        self.config = config

        if init_soap:
            self.document_client, self.document_service = self._build_soap_service(
                config.document_service, config.cache_wsdl  # type: ignore[arg-type]
            )
            self.object_client, self.object_service = self._build_soap_service(
                config.object_service, config.cache_wsdl  # type: ignore[arg-type]
            )
            self.function_client, self.function_service = self._build_soap_service(
                config.function_service, config.cache_wsdl  # type: ignore[arg-type]
            )

    @staticmethod
    def escape_query(val: str) -> str:
        """Escapes the query so that the search works"""

        # replace the first invalid search word with '*'
        val_sp = val.split()
        if val_sp[0].lower() in INV_FIRST_SEARCH_WORDS:
            val_sp[0] = "*"
            val = " ".join(val_sp)

        # replace the invalid characters with *
        val = str(val).replace('"', "''")
        val = val.replace("-", "*")
        val = val.replace(",", "*")

        val = val.replace("'", " ")
        val = val.strip("'")

        logger.debug(f"Search escaped query: {val}")
        return val

    @staticmethod
    def _build_soap_service(
        config: EphSoapService | None,
        cache_wsdl: CacheWsdl,
    ) -> tuple[zeep.client.Client | None, zeep.proxy.ServiceProxy | None]:  # noqa
        if not config:
            return None, None

        session = requests.Session()
        session.headers.update(config.headers)
        transport = zeep.transports.Transport(session=session)

        if cache_wsdl and cache_wsdl.enabled:
            transport.cache = SqliteCache(
                path=cache_wsdl.path, timeout=cache_wsdl.timeout
            )
            logger.info(f"Using cache, configs: {cache_wsdl}")

        settings = zeep.Settings(extra_http_headers=config.headers)
        client = zeep.Client(settings=settings, wsdl=config.wsdl, transport=transport)

        return client, client.service

    @functools.cache
    def get_saksbehandler_pn_id(self) -> int | None:
        if self.config.def_saksbehandler_pn_id is not None:
            return self.config.def_saksbehandler_pn_id
        if self.config.username is None:
            return None
        r = self._do_search(
            object_name="Person",
            filter_exp=f"Brukernavn={ self.config.username }",
            related_objects=["AktivtNavn"],
            eph_resp_object=PersonNavnID,
        )
        if (
            isinstance(r, list)
            and len(r) == 1
            and hasattr(r[0], "AktivtNavn")
            and hasattr(r[0].AktivtNavn, "Id")
        ):
            return int(r[0].AktivtNavn.Id)
        else:
            raise ClientError("PersonNavnId not found")

    def get_filter_expression_from_fields(self, fields: list[tuple[str, str]]) -> str:
        filter_exp = ""
        for field in fields:
            if len(field) != 2:
                raise ValueError("Invalid argument: fields")
            if filter_exp:
                filter_exp = filter_exp + " AND "
            field_name = field[0]
            filtered_val = self.escape_query(field[1])
            if filtered_val.lower() in ["true", "false"] or filtered_val.isdigit():
                filter_exp = filter_exp + f"{field_name}={filtered_val}"
            else:
                filter_exp = filter_exp + f'{field_name}="{filtered_val}"'
        return filter_exp

    def upload_file(self, doc: Doc) -> str:
        """Upload file to EiS and return saved reference"""
        if doc is None:
            raise ClientError("Missing document to upload")

        self.verify_service_is_configured()

        headers = EphUploadHeader(
            ephorte_identity=self.config.document_service.identity,
            storage_identifier="ObjectModelService",
            file_name=doc.name,
        )

        resp = self.document_client.service.UploadFile(
            Content=doc.content, _soapheaders=headers.dict()
        )
        filename: str = resp.header.FileName
        return filename

    def get_document_content_base(
        self,
        document_id: int,
        variant: str = "P",
        version: int = 1,
    ) -> Any:
        """Get file by DocumentId, variant and version."""

        self.verify_service_is_configured()

        headers = EphDocumentContentHeader(
            document_id=document_id,
            identity=self.config.document_service.identity,
            variant=variant,
            version=version,
        )

        resp = self.document_client.service.GetDocumentContentBase(
            _soapheaders=headers.dict()
        )
        return resp

    def create_sak(self, req: CreateSakReq) -> CreateSakResp:
        """Create new sak, only new sak is created when
        having sakId not presented, otherwise journal and documents
        are created for given sakId"""

        self.verify_service_is_configured()

        model_factory = self.object_client.type_factory("ns4")
        do_factory = self.object_client.type_factory("ns1")
        ins = []

        ref_sak, sak = None, None
        if not req.sak_id:
            ref_sak, sak = self.create_sak_obj(do_factory, model_factory, req)
            ins.append(sak)
        else:
            sak = self.fetch_by_id("sak", req.sak_id)
            if sak is None:
                raise ClientError(
                    "Could not find sak, invalid sak_id " + str(req.sak_id)
                )

        journalpost, ref_jp = self.create_journalpost(
            do_factory, model_factory, ref_sak, req
        )
        ins.append(journalpost)
        ins.extend(self.create_multiple_avsender_mottaker(model_factory, ref_jp, req))
        ins.extend(self.create_klasseringer(req, ref_sak))
        ins.extend(
            self.create_multiple_documents(do_factory, model_factory, ref_jp, req)
        )

        resp = self.insert_or_update(ins)

        # Convert resp
        prim_klassering = None
        sek_klassering = None
        if not req.sak_id:
            sak = resp[0]
            jp = resp[1]
            avsender_mottaker = resp[2]
            prim_klassering = resp[3]
            if self.config.sak_config.sek_ordningsprinsipp:
                sek_klassering = resp[4]
        else:
            jp = resp[0]
            avsender_mottaker = resp[1]

        eph_sak = EphSak.from_orm(sak)
        eph_sak.Primaerklassering = EphKlassering.from_orm(prim_klassering)

        if self.config.sak_config.sek_ordningsprinsipp:
            eph_sak.Sekundaerklassering = EphKlassering.from_orm(sek_klassering)

        result = CreateSakResp(
            sak=eph_sak,
            journalpost=EphJournalpost.from_orm(jp),
            avsender_mottaker=EphAvsenderMottaker.from_orm(avsender_mottaker),
        )

        self.patch_sak(req)

        self.handle_post_save(result, req)

        return result

    def sak_patcher(self, req: CreateSakReq) -> dict[str, str | None]:
        """Sak field value to be patched for a sak"""
        return {
            "Tittel": req.tittel,
            "TittelOffentlig": req.off_tittel,
            "ArkivdelId": self.config.search_config.arkivdel_id,
            "Hjemmel": self.config.sak_config.hjemmel,
            "TilgangskodeId": self.config.sak_config.tilgangskode_id,
        }

    def patch_sak(self, req: CreateSakReq) -> Any | None:
        if not req.sak_patch_fields:
            return None

        if not req.sak_id:
            raise ValueError(f"Sak patch fields are defined, but missing sak_id")
        logger.debug("Patching sak..")
        sak = self.fetch_by_id("Sak", req.sak_id)

        patcher = self.sak_patcher(req)
        for field in req.sak_patch_fields:
            if not patcher.get(field):
                raise ValueError(f"Cant patch sak, got invalid field: {field}")
            setattr(sak, field, patcher[field])  # update sak fields

        sak.attr__Id = None
        resp = self.insert_or_update(ins_obj=[sak], update=True)
        return resp

    def get_prim_or_sek(
        self, klass: EphKlassering, prim_prinsipp: str, sek_prinsipp: str
    ) -> tuple[PrinsippName, str, bool]:
        name: PrinsippName
        if klass.OrdningsprinsippId == prim_prinsipp:
            name = "primær"
            prinsipp_id = prim_prinsipp
            unntatt_off = self.config.sak_config.primaer_klassering_untatt_off
        elif klass.OrdningsprinsippId == sek_prinsipp:
            name = "sekundær"
            prinsipp_id = sek_prinsipp
            unntatt_off = self.config.sak_config.sek_klassering_untatt_off
        else:
            raise ValueError(f"Could not find primær or sekundær klassering, {klass}")
        return name, prinsipp_id, unntatt_off

    def create_klasseringer(
        self,
        req: CreateSakReq,
        ref_sak: Any | None = None,
    ) -> list[Any]:
        klasseringer: list[Any] = []
        if req.sak_id:
            if req.sak_patch_fields:
                # when a pre-created-sak, delete existing klassering
                self.delete_klasseringer(req.sak_id)
            else:
                # not needed to create klassering on already created sak (which is not a pre-create-sak)
                return klasseringer

        logger.debug(
            f"Creating klassering sak_id: {req.sak_id}, patch_fields: {req.sak_patch_fields}"
        )
        eph_prim_klassering = self.create_prim_klassering(req, ref_sak)
        klasseringer.append(eph_prim_klassering)

        eph_sek_klassering = self.create_sek_klassering(req, ref_sak)
        if eph_sek_klassering:
            klasseringer.append(eph_sek_klassering)

        logger.debug(f"Creating klasseringer: {klasseringer}")
        return klasseringer

    def create_sek_klassering(
        self, req: CreateSakReq, ref_sak: Any | None = None
    ) -> Any:
        model_factory = self.object_client.type_factory("ns4")
        sek_prinsipp = self.config.sak_config.sek_ordningsprinsipp
        if not sek_prinsipp:
            return None
        eph_sek_klassering = self.create_klassering(
            model_factory=model_factory,
            ord_prinsipp_id=sek_prinsipp,
            ord_verdi_id=self.get_ord_verdi_id("sekundær", sek_prinsipp, req),
            beskr=self.get_ord_besk("sekundær", sek_prinsipp, req),
            untatt_off=self.config.sak_config.sek_klassering_untatt_off,
            req=req,
            ref_sak=ref_sak,
        )
        return eph_sek_klassering

    def create_prim_klassering(
        self, req: CreateSakReq, ref_sak: Any | None = None
    ) -> Any:
        model_factory = self.object_client.type_factory("ns4")
        prim_prinsipp = self.config.sak_config.primaer_ordningsprinsipp
        eph_prim_klassering = self.create_klassering(
            model_factory=model_factory,
            ord_prinsipp_id=prim_prinsipp,
            ord_verdi_id=self.get_ord_verdi_id("primær", prim_prinsipp, req),
            beskr=self.get_ord_besk("primær", prim_prinsipp, req),
            untatt_off=self.config.sak_config.primaer_klassering_untatt_off,
            req=req,
            ref_sak=ref_sak,
        )
        return eph_prim_klassering

    def create_avsender_mottaker(
        self,
        model_factory: zeep.client.Factory,
        ref_jp: Any,
        req: CreateSakReq,
        avs_mot_patch: AvsenderMottakerPatch | None = None,
    ) -> Any:
        eph_av_mot = EphAvsenderMottaker(
            EPostAdresse=req.person.email,
            Journalpost=ref_jp,
            Navn=req.person.short_name(),
            Person=True,
            Postadresse=req.person.post_address,
            Poststed=req.person.post_place,
            Postnummer=req.person.post_code,
            VarsleMedEPost=req.person.email_notify,
            UntattOffentlighet=self.config.jp_config.avsender_untatt_off,
            JournalEnhetId=req.adm_enhet.journal_enhet_id,
        )  # noqa

        # Apply patch
        if avs_mot_patch is not None:
            for field_name in avs_mot_patch.__fields_set__:
                field_value = getattr(avs_mot_patch, field_name)
                setattr(eph_av_mot, field_name, field_value)

        avsender_mottaker = model_factory.AvsenderMottaker(**eph_av_mot)  # noqa
        return avsender_mottaker

    def create_multiple_avsender_mottaker(
        self,
        model_factory: zeep.client.Factory,
        ref_jp: Any,
        req: CreateSakReq,
    ) -> list[Any]:
        avs_mots = []
        for avs_mot_patch in req.avsender_mottaker_patch or [None]:  # type: ignore[list-item]
            avs_mot = self.create_avsender_mottaker(
                model_factory=model_factory,
                ref_jp=ref_jp,
                req=req,
                avs_mot_patch=avs_mot_patch,
            )
            avs_mots.append(avs_mot)

        return avs_mots

    def create_journalpost(
        self,
        do_factory: zeep.client.Factory,
        model_factory: zeep.client.Factory,
        ref_sak: Any,
        req: CreateSakReq,
    ) -> tuple[Any, Any]:
        if self.config.jp_config is None:
            raise ValueError("Cannot create jp, jp config is missing")

        req.apply_def_config(self.config.jp_config, "jp_patch")

        eph_journalpost = EphJournalpost(
            AdministrativEnhetId=req.adm_enhet.id,
            JournalEnhetId=req.adm_enhet.journal_enhet_id,
            Dokumentdato=req.dokument_dato.isoformat(),
            AvsenderMottaker=req.person.first_name,
            AvsenderMottakerUntattOffentlighet=self.config.jp_config.avsender_untatt_off,
            Innholdsbeskrivelse=req.dokument_beskrivelse,
            DokumenttypeId=req.jp_patch.dokument_type_id,
            DokumentkategoriId=req.jp_patch.dokumentkategori_id,
            Hjemmel=req.jp_patch.hjemmel,
            TilgangskodeId=req.jp_patch.tilgangskode_id,
            JournalstatusId=req.jp_patch.status,
            ArkivdelId=self.config.jp_config.arkivdel_id,
            SaksbehandlerId=DEF_JP_SAKSBEHANDLER,
            Tilleggsattributt1=req.jp_patch.tilleggsattributt1,
            Tilleggsattributt2=req.jp_patch.tilleggsattributt2,
            Tilleggsattributt3=req.jp_patch.tilleggsattributt3,
            Tilleggsattributt4=req.jp_patch.tilleggsattributt4,
            Tilleggsattributt5=req.jp_patch.tilleggsattributt5,
            Tilleggsattributt10=req.jp_patch.tilleggsattributt10,
            Endret=None,
            Opprettet=datetime.now(),
            Sak=ref_sak,
            attr__Id="jp1",
        )
        journalpost = model_factory.Journalpost(**eph_journalpost)  # noqa
        ref_jp = do_factory.DataObject(Ref="jp1")

        if req.sak_id is not None:
            journalpost.SakId = req.sak_id
            journalpost.Sak = None
        return journalpost, ref_jp

    def create_sak_obj(
        self,
        do_factory: zeep.client.Factory,
        model_factory: zeep.client.Factory,
        req: CreateSakReq,
    ) -> tuple[Any, Any]:
        if self.config.sak_config is None:
            raise ValueError("Cannot create sak, sak config is missing")

        req.apply_def_config(self.config.sak_config, "sak_patch")

        enhet = model_factory.AdministrativEnhet(
            Id=req.adm_enhet.id, JournalEnhetId=req.adm_enhet.journal_enhet_id
        )

        eph_sak = EphSak(
            AnsvarligEnhet=enhet,
            ArkivdelId=self.config.search_config.arkivdel_id,
            Hjemmel=req.sak_patch.hjemmel,
            TilgangskodeId=req.sak_patch.tilgangskode_id,
            TilgangsgruppeId=req.sak_patch.tilgangsgruppe_id,
            Tittel=req.tittel,
            TittelOffentlig=req.off_tittel,
            JournalEnhetId=req.adm_enhet.journal_enhet_id,
            SaksstatusId=req.sak_patch.status_id,
            attr__Id="sak1",
        )

        sak = model_factory.Sak(**eph_sak)  # noqa
        ref_sak = do_factory.DataObject(Ref="sak1")
        return ref_sak, sak

    def get_klasseringer_by_sak_id(self, sak_id: str | int) -> list[Any]:
        query = "SakId = {}".format(sak_id)

        klasseringer = self._do_search(query, "Klassering", EphKlassering)
        return klasseringer

    @staticmethod
    def create_klassering(
        model_factory: zeep.client.Factory,
        ord_prinsipp_id: str,
        ord_verdi_id: str,
        beskr: str,
        untatt_off: bool,
        req: CreateSakReq,
        ref_sak: Any | None,
    ) -> Any:
        klassering = EphKlassering(
            Beskrivelse=beskr,
            OrdningsprinsippId=ord_prinsipp_id,
            OrdningsverdiId=ord_verdi_id,
            UntattOffentlighet=untatt_off,
        )

        if ref_sak:
            klassering.Sak = ref_sak
        else:
            klassering.SakId = req.sak_id

        ret = model_factory.Klassering(**klassering)  # noqa
        return ret

    def handle_post_save(self, result: CreateSakResp, req: CreateSakReq) -> None:
        if (
            not self.get_saksbehandler_pn_id()
            and not self.config.jp_config.avskriving_maate
        ):
            logger.warning(
                f"Not configured with saksbehandler_pn_id {self.get_saksbehandler_pn_id()} "
                f"and patch.tilgangskode_id, Not handling any post save actions"
            )
            return
        if result.sak.Id is None:
            raise ValueError("Missing sak id after saving")
        if result.journalpost.Id is None:
            raise ValueError("Missing jp id after saving")
        sak = self.fetch_by_id("Sak", result.sak.Id)
        jp = self.fetch_by_id("Journalpost", result.journalpost.Id)
        old_ams = self._do_search(
            f"JournalpostId={result.journalpost.Id}",
            "AvsenderMottaker",
            EphAvsenderMottaker,
        )
        try:
            jp.SaksbehandlerId = self.get_saksbehandler_pn_id()
            # only assign saksbehandler for created sak
            if not req.sak_id:
                sak.SaksansvarligPersonId = self.get_saksbehandler_pn_id()

            jp.JournalstatusId = self.config.jp_config.status
            jp.attr__Id = None
            sak.attr__Id = None
            self.insert_or_update(ins_obj=[jp, sak], update=True)

        except Exception as fa:
            if fa.args and ERR_MSG_SAKSBHEANDLER in fa.args[0]:
                err_msg = self.get_err_msg(req, sak)
                logger.warning(err_msg, exc_info=fa)
                # OK
            else:
                msg = f"Can't set saksbehandler, {sak.Id}, error: " + str(fa)
                logger.error(msg, exc_info=fa)
                raise ValueError(msg)

        if self.config.jp_config.avskriving_maate:
            self.avskriv_restansen(
                result.journalpost.Id,
                self.config.jp_config.avskriving_maate,
            )
        self.update_avs_mot(
            result.journalpost.Id,
            old_ams,
        )

    def avskriv_restansen(
        self, jp_id: int | None, value: str | None
    ) -> EphAvsenderMottaker | None:
        har_rest_query = "HarRestanse = true"

        am_resp = self._do_search(
            f"JournalpostId={jp_id} AND {har_rest_query}",
            "AvsenderMottaker",
            EphAvsenderMottaker,
        )

        if not am_resp:
            logger.info(f"jp_id: {jp_id}, already have 'avskriv restansen'")
            return None
        try:
            logger.info(f"jp_id: {jp_id}, avskriv restansen")
            am = am_resp[0]
            am = self.fetch_by_id("AvsenderMottaker", am.Id)  # noqa
            am = EphAvsenderMottaker.from_orm(am)
            model_factory = self.object_client.type_factory("ns4")
            am = model_factory.AvsenderMottaker(**am)  # noqa
            am.AvskrivningsmaateId = value

            am_upd_resp = self.insert_or_update(am, True)
            if not am_upd_resp:
                raise ValueError("Could not update avsenderMottaker")
        except ClientError as err:
            logger.error(f"Can't avskriv restansen jp_id: {jp_id}", exc_info=err)

        return EphAvsenderMottaker.from_orm(am_upd_resp[0])

    def update_avs_mot(
        self,
        jp_id: int,
        old_ams: list[EphAvsenderMottaker],
    ) -> list[EphAvsenderMottaker]:
        old_ams_map = dict()
        for am in old_ams:
            if am.Id is None:
                logger.error("JP %s has avsenderMottaker with no id", jp_id)
                continue
            if (
                am.AdministrativEnhetId == 0
                and am.SaksbehandlerId == 0
                # and am.JournalEnhetId is None  # TODO
            ):
                # This is an ekstern AvsenderMottaker, don't change it
                continue
            old_ams_map[am.Id] = am

        ams = self._do_search(
            f"JournalpostId={jp_id}",
            "AvsenderMottaker",
            EphAvsenderMottaker,
        )
        ams_map = dict()
        for am in ams:
            if am.Id is None:
                logger.error("JP %s has AvsenderMottaker with no id", jp_id)
                continue
            if (
                am.AdministrativEnhetId == 0
                and am.SaksbehandlerId == 0
                # and am.JournalEnhetId is None  # TODO
            ):
                # This is an ekstern AvsenderMottaker, don't change it
                continue
            ams_map[am.Id] = am

        ams_to_update = []
        model_factory = self.object_client.type_factory("ns4")
        for am_id, am in ams_map.items():
            am = model_factory.AvsenderMottaker(**am)
            if am.SaksbehandlerId == self.get_saksbehandler_pn_id():
                continue
            if self.config.jp_config.set_saksbehandler_id_on_avsmot:
                am.SaksbehandlerId = self.get_saksbehandler_pn_id()
            else:
                old_am = old_ams_map[am_id]
                if (
                    am.SaksbehandlerId
                    or not old_am.SaksbehandlerId
                    or am.SaksbehandlerId == old_am.SaksbehandlerId
                ):
                    continue
                am.SaksbehandlerId = old_am.SaksbehandlerId
            ams_to_update.append(am)
        if not ams_to_update:
            logger.info("No AvsenderMottaker to update for JP %s", jp_id)
            return []

        try:
            ams_upd_resp = self.insert_or_update(ams_to_update, True)
            if not ams_upd_resp:
                raise ValueError(f"Could not update AvsenderMottakers, jp_id '{jp_id}'")
        except ClientError:
            raise ClientError(f"Failed to update AvsenderMottakers, jp_id '{jp_id}'")
        return [
            EphAvsenderMottaker.from_orm(am_upd_resp) for am_upd_resp in ams_upd_resp
        ]

    def insert_or_update(self, ins_obj: list[Any], update: bool = False) -> Any:
        type_factory = self.object_client.type_factory("ns1")
        af_data_objects = type_factory.ArrayOfDataObject(ins_obj)
        identity = self.config.object_service.identity.dict()
        try:
            if update:
                resp = self.object_service.Update(
                    dataObjects=af_data_objects, identity=identity
                )

            else:
                resp = self.object_service.Insert(
                    dataObjects=af_data_objects, identity=identity
                )

        except zeep.exceptions.Fault as ex:
            raise ClientError("Create/Update Failed, " + ex.message)
        return resp

    def search_jp_by_field(
        self, field_name: str, field_val: str
    ) -> list[EphJournalpost]:
        field_val = self.escape_query(field_val)
        filter_exp = f"{field_name} = '{field_val}'"

        return self._do_search(filter_exp, "Journalpost", EphJournalpost)

    def _split_date_interval(
        self,
        date_from: date,
        date_to: date,
    ) -> list[tuple[date, date]]:
        diff = (date_to - date_from).days
        if diff == 0:
            return []
        mid_date = date_from + timedelta(days=int(diff / 2))
        return [(date_from, mid_date), (mid_date + timedelta(days=1), date_to)]

    def search_jp_by_date(
        self,
        fields: list[tuple[str, str]],
        date_from: date | None = None,
        date_to: date | None = None,
    ) -> list[EphJournalpost]:
        """
        Search for Journalposts with (field_name, field_value) in given list of tuples
        created within the date interval [date_from, date_to]
        """

        if date_from is None and date_to is None:
            raise ValueError(f"Missing date_from: {date_from} or date_to: {date_to}")
        if date_from is not None and date_to is not None and not (date_from <= date_to):
            raise ValueError(
                f"date_from ({date_from}) must be before date_to ({date_to})"
            )

        filter_exp = ""
        for field in fields:
            if len(field) != 2:
                raise ValueError("Invalid argument: fields")
            if filter_exp:
                filter_exp += " AND "
            field_name = field[0]
            filtered_val = self.escape_query(field[1])
            filter_exp += f"{field_name}='{filtered_val}'"

        if date_from is not None:
            str_date_from = date_from.strftime("%Y.%m.%d")
            if filter_exp:
                filter_exp += " AND "
            filter_exp += f"Opprettet >= '{str_date_from}'"
        if date_to is not None:
            # We need to include an extra day in the search
            # for it to include date_to, even if we use '<='.
            date_to_ = date_to + timedelta(days=1)
            str_date_to = date_to_.strftime("%Y.%m.%d")
            if filter_exp:
                filter_exp += " AND "
            filter_exp += f"Opprettet <= '{str_date_to}'"

        try:
            return self._do_search(filter_exp.strip(), "Journalpost", EphJournalpost)
        except ClientError as e:
            if (
                "Filtered count query failed. Maximum number of rows reached"
                not in str(e)
                or date_from is None
            ):
                raise
            # Recursively split the search interval in two untill
            # the searches no longer fails, then combine the lists.
            date_intervals = self._split_date_interval(
                date_from,
                date_to or date.today(),
            )
            if not date_intervals:
                return []
            first_interval = date_intervals[0]
            first_search = self.search_jp_by_date(
                fields,
                first_interval[0],
                first_interval[1],
            )
            second_interval = date_intervals[1]
            second_search = self.search_jp_by_date(
                fields,
                second_interval[0],
                second_interval[1],
            )
            combined_search = first_search + second_search
            return combined_search

    def search_ephdokumentreferanse_by_fields(
        self, fields: list[tuple[str, str]]
    ) -> list[EphDokumentreferanse]:
        filter_exp = self.get_filter_expression_from_fields(fields)
        related_objects = ["Journalpost", "Dokumentbeskrivelse"]
        return self._do_search(
            filter_exp=filter_exp,
            object_name="Dokumentreferanse",
            eph_resp_object=EphDokumentreferanse,
            related_objects=related_objects,
        )

    def search_avsendermottaker_by_fields(
        self, fields: list[tuple[str, str]]
    ) -> list[EphDokumentreferanse]:
        filter_exp = self.get_filter_expression_from_fields(fields)
        return self._do_search(
            filter_exp=filter_exp,
            object_name="AvsenderMottaker",
            eph_resp_object=EphAvsenderMottaker,
        )

    def search_dokumentversion_by_field(
        self, fields: list[tuple[str, str]]
    ) -> list[EphDokumentversjon]:
        filter_exp = self.get_filter_expression_from_fields(fields)
        return self._do_search(
            filter_exp=filter_exp,
            object_name="Dokumentversjon",
            eph_resp_object=EphDokumentversjon,
        )

    def _do_search(
        self,
        filter_exp: str,
        object_name: str,
        eph_resp_object: Any,
        sort: str | None = None,
        count: int | None = None,
        related_objects: list[str] | None = None,
    ) -> list[Any]:
        """General search methods that performs search,
        may raise Client error when search fails.
        Returns the type of eph_resp_object or empty array."""

        if not issubclass(eph_resp_object, EphModel):
            raise ValueError("Invalid response return type, should be one of EphModel")

        type_factory = self.object_client.type_factory("ns1")
        query_arg = type_factory.FilteredQueryArguments(
            DataObjectName=object_name,
            FilterExpression=filter_exp,
            ReturnTotalCount=True,
            SortExpression=sort,
        )
        if related_objects:
            query_arg["RelatedObjects"] = self.object_client.get_type(
                "ns2:ArrayOfstring"
            )(related_objects)
        if count:
            query_arg["TakeCount"] = count
        try:
            search_resp = self.object_service.FilteredQuery(
                arguments=query_arg,
                identity=self.config.document_service.identity.dict(),
            )
        except zeep.exceptions.Fault as e:
            raise ClientError(f"Search failed, {e.message}")
        eph_resp_obj_list = []
        if search_resp.DataObjects:
            eph_resp_obj_list = [
                eph_resp_object.from_orm(x) for x in search_resp.DataObjects.DataObject
            ]
        return eph_resp_obj_list

    def search_admenheter_by_kortnavn(
        self, kortnavn: str
    ) -> list[EphAdministrativEnhet]:
        kortnavn = self.escape_query(kortnavn)
        filter_exp = f"kortnavn = '{kortnavn}'"

        return self._do_search(filter_exp, "AdministrativEnhet", EphAdministrativEnhet)

    def search_created_sak(self, saksaar: int, sekvensnummer: int) -> EphSak | None:
        """Sak search by saksaar and sekvensnummer, expected to return One Sak or None"""

        query = "Saksaar={} and Sekvensnummer={} and SaksstatusId != U and SaksstatusId != A".format(
            saksaar, sekvensnummer
        )
        saker = self._do_search(self.escape_query(query), "Sak", EphSak)
        if not saker:
            return None
        if len(saker) != 1:
            raise ValueError(f"Expected 1 but got {len(saker)}, query: {query}")

        return saker[0]  # type: ignore[no-any-return]

    def search_sak(self, title: str, emp_nr: str, adm_enh_id: int) -> list[EphSak]:
        """Sak search by name, emp_nr and adm_enh_id."""
        self.verify_service_is_configured()

        ord_verdi_id: str | None = emp_nr
        if self.config.interface == "uit":
            ord_verdi_id = self.config.sak_config.primaer_ordningsprinsipp_id
        else:
            title = title.split(" ")[-1]  # remove names in sak tittle when searching
        search = Search(
            title=title,
            adm_enh_id=adm_enh_id,
            status_avsluttet=self.config.search_config.status_avsluttet,
            status_utgaat=self.config.search_config.status_utgaat,
            prim_ord_prinsipp=self.config.sak_config.primaer_ordningsprinsipp,
            sek_ord_prinsipp=self.config.sak_config.sek_ordningsprinsipp,
            ord_verdi_id=ord_verdi_id,
        )
        prinsipp_id, prinsipp_name, untatt_off = self.get_prinsipp_by_interface()

        # empty enhet query for uit
        enhet_query = f"AND SaksansvarligEnhetId ='{search.adm_enh_id}'"
        ord_verdi_query = (
            f"AND {prinsipp_name}.OrdningsverdiId = '{search.ord_verdi_id}'"
        )
        if "uit" in self.config.interface:
            enhet_query = ""

        query = (
            "tittel = '{}' "
            "{} "
            "AND SaksstatusId != '{}' AND SaksstatusId != '{}' "
            "AND {}.OrdningsprinsippId = '{}' "
            "{}".format(
                self.escape_query(title),
                enhet_query,
                search.status_avsluttet,
                search.status_utgaat,
                prinsipp_name,
                prinsipp_id,
                ord_verdi_query,
            )
        )
        logger.debug(f"Search sak query: {query}")
        return self._do_search(query, "Sak", EphSak, "Id desc")

    def get_prinsipp_by_interface(
        self,
    ) -> tuple[str | None, KlasseringPrinsippName, bool]:
        prinsipp_id: str | None = self.config.sak_config.primaer_ordningsprinsipp
        prinsipp_name: KlasseringPrinsippName = "Primaerklassering"
        untatt_off: bool = self.config.sak_config.primaer_klassering_untatt_off
        if "uio" in self.config.interface:
            prinsipp_name = "Sekundaerklassering"
            prinsipp_id = self.config.sak_config.sek_ordningsprinsipp
            untatt_off = self.config.sak_config.sek_klassering_untatt_off
        return prinsipp_id, prinsipp_name, untatt_off

    def get_doc_format(self, doc: Doc) -> str:
        doc_config = self.config.doc_config

        formats: dict[str | None, str] = {
            "application/pdf": doc_config.format_pdf,
            "text/html": doc_config.format_html,
            "text/plain": doc_config.format_txt,
            "image/tiff": doc_config.format_tiff,
        }
        eph_format = formats.get(doc.format)
        if eph_format is not None:
            return eph_format

        doc_formats = guess_type(doc.name)
        eph_format = formats.get(doc_formats[0])
        if eph_format is not None:
            return eph_format

        if "." in doc.name:
            doc_name_ending = doc.name.split(".")[-1]
            eph_format = doc_name_ending
        if eph_format is not None:
            return eph_format.upper()

        raise ValueError(f"Cant get eph format for doc_name: {doc.name}")

    def create_document(
        self,
        do_factory: zeep.client.Factory,
        model_factory: zeep.client.Factory,
        ref_jp: Any,
        doc: Doc,
        doc_besk_id: str,
        hoved_or_vedlegg: str,
    ) -> list[Any]:
        """Create new documents with file uploaded to EiS
        The new document is created for given journalpost"""
        data_obj: list[Any] = []
        if not doc:
            return data_obj

        doc_format = self.get_doc_format(doc)

        doc_ref = self.upload_file(doc)

        eph_beskrivelse = EphDokumentbeskrivelse(
            TilgangskodeId=self.config.doc_config.tilgangs_kode_id,
            Dokumenttittel=doc.title,
            DokumentkategoriId=self.config.doc_config.dokument_kategori_id,
            Papirdokument=False,
            attr__Id=doc_besk_id,
        )

        beskrivelse = model_factory.Dokumentbeskrivelse(**eph_beskrivelse)  # noqa
        ref_beskrivelse = do_factory.DataObject(Ref=doc_besk_id)
        data_obj.append(beskrivelse)

        eph_versjon = EphDokumentversjon(
            Dokumentbeskrivelse=ref_beskrivelse,
            Dokumentreferanse=doc_ref,
            LagringsformatId=doc_format,
            TilgangskodeId=self.config.doc_config.tilgangs_kode_id,
        )
        versjon = model_factory.Dokumentversjon(**eph_versjon)  # noqa
        data_obj.append(versjon)

        eph_referanse = EphDokumentreferanse(
            Dokumentbeskrivelse=ref_beskrivelse,
            Journalpost=ref_jp,
            TilknytningskodeId=hoved_or_vedlegg,
        )
        referanse = model_factory.Dokumentreferanse(**eph_referanse)  # noqa
        data_obj.append(referanse)

        return data_obj

    def create_multiple_documents(
        self,
        do_factory: zeep.client.Factory,
        model_factory: zeep.client.Factory,
        ref_jp: Any,
        req: CreateSakReq,
    ) -> list[Any]:
        docs = req.docs
        if not isinstance(docs, list):
            raise TypeError("Document(s) must be passed as list")

        ret = []
        for doc_besk_id_int, doc in enumerate(docs):
            # Set the first document to hoved-document and the rest to vedlegg-document
            hoved_or_vedlegg = (
                self.config.doc_config.tilknyttning_hoved
                if doc_besk_id_int == 0
                else self.config.doc_config.tilknyttning_vedlegg
            )
            doc_besk_id = f"besk{doc_besk_id_int+1}"
            ret.extend(
                self.create_document(
                    do_factory,
                    model_factory,
                    ref_jp,
                    doc,
                    doc_besk_id,
                    hoved_or_vedlegg,
                )
            )

        return ret

    def delete_by_id(self, db_obj_name: str, id: int | None) -> None:
        """Delete objects from EiS"""
        if not id:
            raise ValueError(f"Can't delete by id, invalid id value: {id}")

        pk = {"Key": "Id", "Value": str(id)}
        do_factory = self.object_client.type_factory("ns1")
        pk_col = do_factory.PrimaryKeyCollection(PrimaryKey=[pk])

        try:
            self.object_service.DeleteById(
                dataObjectName=db_obj_name,
                identity=self.config.object_service.identity.dict(),
                primaryKeys=pk_col,
            )
        except zeep.exceptions.Fault as ex:
            raise ClientError(f"Delete of {db_obj_name}({id}) failed, {ex.message}")

    def fetch_by_id(self, db_obj_name: str, id: int) -> Any:
        """Fetches objects from EiS"""
        if not id:
            raise ValueError(f"Can't fetch by id, invalid id value: {id}")

        pk = {"Key": "Id", "Value": str(id)}
        do_factory = self.object_client.type_factory("ns1")
        pk_col = do_factory.PrimaryKeyCollection(PrimaryKey=[pk])

        fetch_arg = do_factory.FetchArguments(
            DataObjectName=db_obj_name, PrimaryKeys=pk_col
        )
        try:
            fetch_resp = self.object_service.Fetch(
                arguments=fetch_arg, identity=self.config.object_service.identity.dict()
            )
            result = fetch_resp.DataObject
        except zeep.exceptions.Fault as ex:
            if ex.message == "Forespurt data finnes ikke i databasen":  # eis_error msg
                return None
            raise ClientError(f"Fetch of {db_obj_name}({id}) failed, {ex.message}")

        return result

    def get_administrativ_enhet_by_id(self, id: str) -> EphAdministrativEnhet | None:
        try:
            int(id)
        except ValueError:
            raise ClientError(f'Invalid id "{id}", must be an integer.')
        orm_obj = self.fetch_by_id("AdministrativEnhet", id)  # type: ignore[arg-type]
        if orm_obj is None:
            return None
        return EphAdministrativEnhet.from_orm(orm_obj)

    def update_list_of_fields(
        self, journalpost_id: int, fields_to_update: dict[str, Any]
    ) -> Any | None:
        jp = self.fetch_by_id("Journalpost", journalpost_id)
        jp.Hjemmel = fields_to_update["hjemmel"]
        jp.TilgangskodeId = fields_to_update["tilgangskode_id"]
        jp.TilgangsgruppeId = fields_to_update["tilgangsgruppe_id"]
        jp.AvsenderMottakerUntattOffentlighet = fields_to_update[
            "avsender_mottaker_untatt_offentlighet"
        ]
        resp = self.insert_or_update(ins_obj=[jp], update=True)
        return resp

    def get_ord_verdi_id(self, prefix: str, prinsipp: str, req: CreateSakReq) -> str:
        if prinsipp == PrinsippType.ansatt_nr.value or prinsipp == PrinsippType.anr:
            return req.person.emp_nr
        elif prinsipp == PrinsippType.arknok.value:
            return req.sek_klassering.ordnings_verdi_id
        elif prinsipp == PrinsippType.arkuit.value:
            if not self.config.sak_config.primaer_ordningsprinsipp_id:
                raise ValueError(
                    "Required config: <primaer_ordningsprinsipp_id> is missing"
                )
            return self.config.sak_config.primaer_ordningsprinsipp_id
        elif prinsipp == PrinsippType.emne.value:
            return req.sek_klassering.ordnings_verdi_id
        elif prinsipp == PrinsippType.fdato.value:
            # For example from "1970-01-02" to "02011970"
            return date.fromisoformat(req.person.birth_date).strftime("%d%m%Y")
        elif prinsipp == PrinsippType.fnra.value:
            if req.person.fnr is None:
                raise ValueError("Person is missing fnr")
            return req.person.fnr
        else:
            raise ValueError(f"Invalid {prefix}prinsipp: {prinsipp}")

    def get_ord_besk(self, prefix: str, prinsipp: str, req: CreateSakReq) -> str:
        if prinsipp == PrinsippType.ansatt_nr.value or prinsipp == PrinsippType.anr:
            return req.person.name()
        elif prinsipp == PrinsippType.arknok.value:
            return req.sek_klassering.beskrivelse
        elif prinsipp == PrinsippType.arkuit.value:
            if not self.config.sak_config.primaer_ordningsprinsipp_value:
                raise ValueError(
                    "Required config: <primaer_ordningsprinsipp_value> is missing"
                )
            return self.config.sak_config.primaer_ordningsprinsipp_value
        elif prinsipp == PrinsippType.emne.value:
            return req.sek_klassering.beskrivelse
        elif prinsipp == PrinsippType.fdato.value:
            return req.person.name()
        elif prinsipp == PrinsippType.fnra.value:
            return req.person.name()
        else:
            raise ValueError(f"Invalid {prefix}prinsipp: {prinsipp}")

    def verify_service_is_configured(self) -> None:
        if not self.document_service and not self.object_service:
            raise ClientError("object and document API is not configured")

    def get_err_msg(self, req: CreateSakReq, sak: Any) -> str:
        war_msg = (
            f"Could not update sak-id: {sak.Id} and with"
            f" saksbehandler pn_id: {self.get_saksbehandler_pn_id()} does not belong to same"
            f" adm_enhet: {req.adm_enhet}"
        )
        return war_msg

    def delete_klasseringer(self, sak_id: int) -> None:
        """Delete klassering, at deletion of primary klassering may fail, it should tried to delete at last"""
        klasseringer = self.get_klasseringer_by_sak_id(sak_id)
        # delete primary klassering at last
        klasseringer.reverse()
        max_retry = len(klasseringer)
        count = 0
        for kl in klasseringer:
            try:
                self.delete_by_id("Klassering", kl.Id)
            except ClientError as err:
                # OK
                logger.error(f"Could not delete klassering: {klasseringer}")
                # retry to delete klassering
                if count < max_retry:
                    klasseringer.append(kl)
                count = count + 1

    def query_function_descriptors(self) -> Any:
        ephorte_identity = self.config.function_service.identity.dict()
        resp = self.function_service.QueryFunctionDescriptors(identity=ephorte_identity)
        return resp

    def get_template_type(self) -> Any:
        ephorte_identity = self.config.function_service.identity.dict()
        resp = self.function_service.ExecuteFunction(
            identity=ephorte_identity, name="GetTemplateType", parameters=[]
        )
        return resp

    def renummerer_journalpost(self, sak_id: str) -> Any:
        ephorte_identity = self.config.function_service.identity.dict()
        type_factory = self.function_client.type_factory("ns4")

        any_object = zeep.xsd.AnyObject
        xsd_types = zeep.xsd

        sak_id_ns4 = type_factory.ArrayOfanyType(
            [any_object(xsd_types.String(), sak_id)]
        )
        resp = self.function_service.ExecuteFunction(
            identity=ephorte_identity,
            name="RenummererJournalPost",
            parameters=sak_id_ns4,
        )
        return resp

    def besvar_journalpost(
        self,
        besvarer_avsender_mottaker_id: int,
        journalpost_id_det_avskrives_med: int,
        avskrivingsmaate: str,
        journalpost_i_der_som_skal_avskrives: str,
        dokumentkategori: str,
    ) -> Any:
        ephorte_identity = self.config.function_service.identity.dict()
        type_factory = self.function_client.type_factory("ns4")

        any_object = zeep.xsd.AnyObject
        xsd_types = zeep.xsd

        params_ns4 = type_factory.ArrayOfanyType(
            [
                any_object(xsd_types.Int(), besvarer_avsender_mottaker_id),
                any_object(xsd_types.Int(), journalpost_id_det_avskrives_med),
                any_object(xsd_types.String(), avskrivingsmaate),
                any_object(xsd_types.String(), journalpost_i_der_som_skal_avskrives),
                any_object(xsd_types.String(), dokumentkategori),
            ]
        )
        resp = self.function_service.ExecuteFunction(
            identity=ephorte_identity,
            name="BesvarInngaaendeJournalposterMedUtgaaende",
            parameters=params_ns4,
        )
        return resp

    def convert_doc_to_pdf(
        self,
        document_description_id: int,
        version: int,
        variant: str,
        link_to_registry_entry: int,
        check_in_document: bool,
    ) -> Any:
        ephorte_identity = self.config.function_service.identity.dict()
        type_factory = self.function_client.type_factory("ns4")
        any_object = zeep.xsd.AnyObject
        xsd_types = zeep.xsd
        params_ns4 = type_factory.ArrayOfanyType(
            [
                any_object(xsd_types.Int(), document_description_id),
                any_object(xsd_types.Int(), version),
                any_object(xsd_types.String(), variant),
                any_object(xsd_types.Int(), link_to_registry_entry),
                any_object(xsd_types.Boolean(), check_in_document),
            ]
        )

        resp = self.function_service.ExecuteFunction(
            identity=ephorte_identity,
            name="ConvertToPdf",
            parameters=params_ns4,
        )
        return resp

    def remerge_document(
        self,
        registry_entry_id: int,
        document_description_id: int,
    ) -> Any:
        ephorte_identity = self.config.function_service.identity.dict()
        type_factory = self.function_client.type_factory("ns4")
        any_object = zeep.xsd.AnyObject
        xsd_types = zeep.xsd
        params_ns4 = type_factory.ArrayOfanyType(
            [
                any_object(xsd_types.Int(), registry_entry_id),
                any_object(xsd_types.Int(), document_description_id),
            ]
        )

        resp = self.function_service.ExecuteFunction(
            identity=ephorte_identity, name="RemergeDocument", parameters=params_ns4
        )
        return resp


def get_client(config: EphClientConfig) -> EphClient:
    return EphClient(config)
